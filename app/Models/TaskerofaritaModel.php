<?php namespace App\Models;
use CodeIgniter\Model;

class TaskerofaritaModel extends Model
{
  public function get1taskerofarita($id)
  {
    $db = db_connect();
    $strsql = "SELECT * FROM taskerofarita where tfid = ".$id." ";
    $query = $this->db->query($strsql);
    $r = $query->getRow();
    return  $r;
  }

    public function getTaskerofaritaj($p)
    {
        $tid = $p['tid'];
        $userapp = $p['userapp'];
        $useradmin = $p['useradmin'];
        if (isset($p['aadatseldeb'])){
            $aadatseldeb = $p['aadatseldeb'];
        }
        else
        {
            $aadatseldeb ='2000-01-01';
        }
  
        if (isset($p['aadatselfin'])){
            $aadatselfin = $p['aadatselfin'];
        }
        else
        {
            $aadatselfin = date('Y-m-d');
        }
  
      $where = " ";
  
      if ($useradmin != "A")
      {
        $where = " where tfuzanto = '".$userapp."' ";
      }
  
     if ($aadatseldeb > '')
      {
        $where = " where tfdate >= '".$aadatseldeb."'";
      }
  
  
        $where .= " and tfdate <= '".  $aadatselfin."'";
  
  
      if ($tid > 0)
      {
        $where .= " and tftid = ".$tid;
      }
  
      $strsql = "select * from taskerofarita tf left join tasko t on tftid = tid ".$where." order by tfdate desc,tfid desc ";
      //echo $strsql;
      $db = db_connect();
      $query = $db->query($strsql);
      $d = $query->getResult();
      return  $d;
    }

    public function getNbTaskerofaritaj($id)
    {
      // recherche du nombre de réalisation pour une tâche
      
      $strsql = "select count(*) as nbtf from taskerofarita where tftid =".$id;
      //echo $strsql;
      $db = db_connect();
      $query = $db->query($strsql);
      $r = $query->getRow();
      return  $r->nbtf ;
    }
    
    public function getDurationreal($id)
    {
      // recherche du nombre du total réalisé
      $strsql = "select sum(tfduration) as duration from taskerofarita where tftid =".$id;
      //echo $strsql;
      $db = db_connect();
      $query = $db->query($strsql);
      $r = $query->getRow();
      return  $r->duration ;
    }

}