<?php 
namespace App\Models;
use CodeIgniter\Model;
use CodeIgniter\I18n\Time;


class SprintModel extends Model
{
    protected $table = 'sprint';

    public function getSprintj($p)
    {
      $session = \Config\Services::session();

      $db = db_connect();
      $where = " ";
 
      $orderby = " order by datdeb desc";
      $strsql = "select * from sprint ".$where.$orderby;
    // echo $strsql;
      $query = $db->query($strsql);
      $d = $query->getResult();
      return  $d;
    }


    public function get1Sprint($id)
    {
      if ($id > 0){
        $db = db_connect();
        $strsql = "SELECT * FROM sprint where id = ".$id." ";
        $query = $this->db->query($strsql);
        $r = $query->getRow();

      }
      else
      {
       echo 'id du sprint non trouvé';
      }
      return  $r;

    }

    function getSprintDropDown(){
      $session = \Config\Services::session();
       $db = db_connect();
       $strsql = "select * from sprint   order by datdeb";
       $query = $db->query($strsql);
       $data['t'] = $query->getResult();
       return  $data['t'];
    }
/*
    public function calculordre()
    {
      $db = db_connect();
      // calcul de l'ordre pour toutes les tâche non termimés
      $strsql = "select * from projekto where status < '7'";
     // echo $strsql;
      $db = db_connect();
      $query = $db->query($strsql);
      $t = $query->getResult();
      foreach ($t as $r) {
        $this->calcul1ordre($r);
      }

    }

    public function calcul1ordre($r)
    {
      $this->updProjektoFromTasko($r->id);
      // durée estimé en heures - durée réelle (à calculer des tâches) + nombre jours jusqu'à la date échéance + priorité
      $wudate = date('Y-m-d');
      $durationreal = 0;
      if ($r->durationreal > 0) $durationreal = $r->durationreal;
      $durationestim= 0;
      if ($r->durationestim > 0) $durationestim = $r->durationestim;
      // nombre de jour jusqu'à échéance : plus c'est loin dans le temps plus loin sera l'ordre

      // est ce utile ?
      $durationestim= 0;
      if ($r->durationestim > 0) $durationestim = $r->durationestim;
      $termindate=   $wudate ;
      if ($r->termindate > '') $termindate = $r->termindate;

      // si l'échéance est loin dans le future plus le nombre sera élevé
      $hodiau = Time::parse($wudate);
      $termin = Time::parse($wudate);
      if ($r->termindate > '2000-01-01') $termin    = Time::parse($r->termindate);
      $diff = $hodiau->difference($termin);

      $prio = 0;
      if ($r->priorit > 1) $prio = ($r->priorit - 50) * 2 ;
     // echo'<br/>';
     // $ordre = $tdurationestim - $tdurationreal + $diff->days + $prio;
      $ordre =  $durationestim -  $durationreal  +  $diff->days + $prio;
      $db = db_connect();
      $builder = $db->table('projekto');
      $builder->where('id', $r->id);
      $d = [
        'ordre'  => $ordre
      ];
      $builder->update($d);
    }
    
    public function updProjektoFromTasko($id)
    {
      $wudate = date('Y-m-d');
      // durée estimée et réelle = total des durées des tâches
      $strsql = "select sum(tdurationestim) as durationestim, sum(tdurationreal) as durationreal, max(ttermindate) as termindate from tasko where projektoid =".$id;
    //  echo $strsql;
      $db = db_connect();
      $query = $db->query($strsql);
      $r = $query->getRow();
     // $tdurationreal = $r->duration ;
      $builder = $db->table('projekto');
      $builder->where('id', $id);
      $d = [
        'durationestim'  => $r->durationestim,
        'durationreal'  => $r->durationreal,
        'datreal'  =>  $wudate
      ];
      $builder->update($d);
    }
    */
  function CreationTable(){
    echo 'creation table Sprint';
    $db = db_connect();
    $DB_TYPE = getenv('DB_TYPE');
    if ( $DB_TYPE == 'SQLite' )
    {
      $strsql = " CREATE TABLE `sprint` (
        `id` INTEGER,
        `nom` TEXT, -- 'nom du sprint'
        `desc` TEXT,
        `goal` TEXT,
        `review` TEXT,
        `datdeb`	TEXT,
        `datfin`	TEXT,
        `status` TEXT,
        `nb_h_max` INTEGER, -- nombre d'heure maximum
        `datcrt`	TEXT,
    	  `usrcrt`	TEXT,
    	  `datupd`	TEXT,
    	  `usrupd`	TEXT,
         datreal TEXT,
         PRIMARY KEY(`id`))";
  
        echo $strsql;
        $db->query($strsql);
      
    }

    $data['logcode'] = 'Install';
    $data['logtext'] = 'Création table sprint  '.$strsql.'  ';
    $log = new LogModel();
    $log->AddLog($data);
  }
 
}
