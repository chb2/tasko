<?php 
namespace App\Models;
use CodeIgniter\Model;

class LogModel extends Model{

    function CreationTable(){
        // création table utilisateur simple
      /*
        V1:
        logtype
        logdesc 
    */    
        $db = db_connect();

        $DB_TYPE = getenv('DB_TYPE');
        if ( $DB_TYPE == 'SQLite' )
        {
          $strsql = " CREATE TABLE IF NOT EXISTS `log` (
            `id` INTEGER, 
            logcode varchar(30) NOT NULL,
            logtext text ,
            datcrt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            usrcrt varchar(30) NOT NULL ,
            datmod datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
           usrmod varchar(30) NOT NULL , 
       PRIMARY KEY(`id`) ) ";  
        }

        if ( $DB_TYPE == 'MySQL' )
        {
        $strsql = "CREATE TABLE IF NOT EXISTS log (
            id int(8) NOT NULL AUTO_INCREMENT,
            logcode varchar(30) NOT NULL,
            logtext text ,
            datcrt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            usrcrt varchar(30) NOT NULL ,
            datmod datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            usrmod varchar(30) NOT NULL ,
            PRIMARY KEY (id)
            ) ENGINE=InnoDB CHARSET=utf8mb4;";
        }
            // à remplacer dans un log
            echo $strsql;
            $db->query($strsql);
        // V2 type (ex moderateur) cletype (user)  

    }
    function AddLog($data){
      //  echo 'addlog';
        $session = \Config\Services::session();

        if (!isset($session->userapp))
        {
            $session->userapp = '...';
        }
        $d['logcode'] = $data['logcode'];
        $d['logtext'] = $data['logtext'];
        $d['usrcrt'] = $session->userapp;
        $d['usrmod'] = $session->userapp;
        $db = db_connect();
        $db->table('log')->insert($d);
         //       echo 'fin addlog';
    }


}

/*function addlog($fileprefix, $logtype, $logdesc)
{
    V1:
    logtype
    logdesc 

    global $mysqli;
    $table = $fileprefix."log";
    $wudate = date('Y-m-d');
    $whms = date('G:i:s');
	    $strsql = "INSERT INTO $table (logtype,logdesc, logdatcrt,logtimcrt,logusrcrt) Values('".$logtype."','".$logdesc."','".$wudate."','".$whms."','".$_SESSION['uticode']."')";

    //echo $strsql;
    //mysql_query($strsql);
    $mysqli->query($strsql);
}

function addlog2($data)
{
   // echo 'addlog2 fonction<br/>';
   
    global $mysqli;
    $table = $data['fileprefix']."log";

    $wudate = date('Y-m-d');
    $whms = date('G:i:s');
    //echo "prefix ".$data['fileprefix'];
    //echo "<br/>logtype".$data['logtype'];
    $strsql = "INSERT INTO ".$table." (logtype,logcletype, logcode1,logdesc,logdatcrt,logtimcrt,logusrcrt) Values('".$data['logtype']."',".$data['logcletype'].",'".$data['logcode1']."','".$data['logdesc']."','".$wudate."','".$whms."','".$_SESSION['uticode']."')";
    //$strsql = "INSERT INTO ".$table." (logtype,logcletype, logdatcrt,logtimcrt,logusrcrt) Values('".$data['logtype']."',".$data['logcletype'].", ";
   // echo $strsql;

    //mysql_query($strsql);
    $mysqli->query($strsql);

    v2
    logtype
    logcletype ?

    			$data['fileprefix'] = $fileprefix;
				$data['logtype'] = 'moderateur';
				$data['logcletype'] = $row['usid'];
				$data['logcode1'] = 'connexion';
				$data['logdesc'] = $logdesc;
				addlog2($data);

}
*/
