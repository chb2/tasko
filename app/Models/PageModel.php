<?php 
namespace App\Models;
use CodeIgniter\Model;

class PageModel extends Model{

    public function affiche($data,$page = 'home')
    {
        $page = $data['page']; 
        if ( ! is_file(APPPATH.'/Views/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }

        $data['title'] = ucfirst($page); 
        echo view('template/header', $data);
        echo view($page.'.php', $data);
       echo view('template/footer', $data);
    }

    public function page($data)
    {
        $page = $data['page']; 
        if ( ! is_file(APPPATH.'/Views/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }

        $data['title'] = ucfirst($page); 
        echo view('template/header', $data);
        echo view($page.'.php', $data);
        echo view('template/footer', $data);
    }

    public function gestion($data,$page = 'home')
    {

        echo view('gestion/template/header', $data);
        echo view('gestion/'.$page.'.php', $data);
        echo view('gestion/template/footer', $data);
    }

  
}
