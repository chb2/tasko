<?php 
namespace App\Models;
use CodeIgniter\Model;
use CodeIgniter\I18n\Time;



class TaskoModel extends Model
{
    
  public function get1tasko($id)
  {
    $db = db_connect();
    $strsql = "SELECT * FROM tasko where tid = ".$id." ";
    $query = $this->db->query($strsql);
    $r = $query->getRow();
    return  $r;
  }

  
  
  public function getTaskoj($p)
    {
      //echo 'userapp'.$p['userapp'];
      $order =  'ordre';
      if (isset($p['order'])) {
        $order = $p['order'];
    }
      $session = \Config\Services::session();

      $db = db_connect();
      $where = "where tid > 0 ";
    /*  if ($session->useradmin <> 'A')
      {
        $where .= " and tuzanto = '".$p['userapp']."'";
      }*/

      $listestatus = array("3", "5", "7");
      if (in_array($p['tstatus'], $listestatus)) {
         $where.= " and tstatus = '".$p['tstatus']."'";
      }
      $orderby = " order by ordre,tdatreal desc, tid desc";
      if ($order == 'tdatreal') {
        $orderby = " order by tdatreal desc, tid desc";
      }
      $strsql = "select * from tasko ".$where.$orderby;
    // echo $strsql;
      $query = $db->query($strsql);
      $d = $query->getResult();
      return  $d;
    }
  function getTaskosProjekto($projektoid){
    $db = db_connect();
    $strsql = "select * from tasko where projektoid = ".$projektoid;
    $strsql .= " order by tstatus" ;
    //echo $strsql;
    $query = $db->query($strsql);
    return $query->getResult();
    //return  $d;
}
function getTaskosSprint($sprintid){
  $db = db_connect();
  $strsql = "select * from tasko where sprintid = ".$sprintid;
  $strsql .= " order by tstatus,ordre" ;
  //echo $strsql;
  $query = $db->query($strsql);
  return $query->getResult();
  //return  $d;
}
function getStatusProjekto($projektoid){
  $db = db_connect();
  $strsql = "select min(tstatus) from tasko where projektoid = ".$projektoid;
  //echo $strsql;
  $query = $db->query($strsql);
  $r = $query->getRow();
  //  return $query->getResult();
  //return  $d;
}

function getNbTaskosProjekto($projektoid){
  $db = db_connect();
  $strsql = "select count(*) as nb from tasko where projektoid = ".$projektoid;
  $query = $db->query($strsql);
  $e = $query->getRow();
  return $e->nb;
}
    function getTaskoDropDown($p){
      $session = \Config\Services::session();
       $db = db_connect();
       $strsql = "select * from tasko";
       $where = " where tstatus = '5' ";
      // if ($p['useradmin'] <> 'A')
       if ($session->useradmin <> 'A')
       {
         $where .= " and tuzanto = '".$p['userapp']."'";
       }
       $orderby = " order by tdatreal desc, tid desc";
       $strsql = "select * from tasko ".$where.$orderby; 
  //   echo $strsql;
       $query = $db->query($strsql);
       $data['t'] = $query->getResult();
       return  $data['t'];
    }

    public function updDurationreal($id)
    {
      // recherche du nombre du total réalisé
      $strsql = "select sum(tfduration) as duration from taskerofarita where tftid =".$id;
      //echo $strsql;
      $db = db_connect();
      $query = $db->query($strsql);
      $r = $query->getRow();
      $tdurationreal = $r->duration ;
      $builder = $db->table('tasko');
      $builder->where('tid', $id);
      $d = [
        'tdurationreal'  => $r->duration
      ];
      $builder->update($d);
    }

    public function updDateReal($id,$datereal)
    {
      // mise à jour de la date de réalisaiton
      $wudate = date('Y-m-d');
      $db = db_connect();
     // echo 'mise a jour '.$id;
     // echo 'date '.$datereal;
      $builder = $db->table('tasko');
      $builder->where('tid', $id);
      $d = [
        'tdatreal'  =>  $wudate
      ];
      $builder->update($d);
    }

    public function calculordre()
    {
      $db = db_connect();
      // calcul de l'ordre pour toutes les tâche non termimés
      $strsql = "select * from tasko where tstatus < '7'";
     // echo $strsql;
      $db = db_connect();
      $query = $db->query($strsql);
      $t = $query->getResult();
      foreach ($t as $r) {
        $this->calcul1ordre($r);

      }

    }

    public function getStatusName($statuscode)
    {
      $statusName = '';
      if ($statuscode == "3" ){$statusName = 'A venir';}
      if ($statuscode == "5" ){$statusName = 'En cours';}
      if ($statuscode == "7" ){$statusName = 'Cloturé';}
      return $statusName;
    }

    public function calcul1ordre($r)
    {
      // méthode de calcul
      // nombre de jour jusqu'à l'échéance
      // plus nombre d'heures restant
      $id = $r->tid;
      $wudate = date('Y-m-d');
      $tdurationreal = 0;
      if ($r->tdurationreal > 0) $tdurationreal = $r->tdurationreal;
      $tdurationestim= 0;
      if ($r->tdurationestim > 0) $tdurationestim = $r->tdurationestim;
      $ttermindate=   $wudate ;
      if ($r->ttermindate > '') $ttermindate = $r->ttermindate;

      // si l'échéance est loin dans le future plus le nombre sera élevé
      $hodiau = Time::parse($wudate);
      $termin    = Time::parse($r->ttermindate);

      $diff = $hodiau->difference($termin);
     // echo 'nb jours diffférence entre '.$wudate. ' et '. $ttermindate. ' '.$diff->days;    // -2557

     // echo 'Time '.$time;


    // echo " ordre ".$ordre;
    $prio = 0;
    if ($r->tpriorit > 1) $prio = ($r->tpriorit - 50) * 2 ;
  //  $ordre = Time::parse($r->ttermindate);

    $ordre = $tdurationestim - $tdurationreal + $diff->days + $prio;
      $db = db_connect();
      $builder = $db->table('tasko');
      $builder->where('tid', $id);
      $d = [
        'ordre'  => $ordre
      ];
      $builder->update($d);

    }

  public function addfield($field, $type){
		$db = db_connect();
		$strsql = "ALTER TABLE tasko  ADD $field $type ";
		echo '<br/>'.$strsql;
		try {	 $db->query($strsql); }
		 catch (\Exception $e)	 { 	 echo "<br/>Column $field already exists ";	 }

	}

  public function cloturer($id)
  {
    $db = db_connect();
    $builder = $db->table('tasko');
    $builder->where('tid', $id);
    $d = [
      'tstatus'  => "7"
    ];
    $builder->update($d);
  }

  public function commencer($id)
  {
    // passage à status 5 et date d'échéance 14 jours
    $db = db_connect();
    $builder = $db->table('tasko');
    $builder->where('tid', $id);
    $d = [
      'tstatus'  => "5",
      'ttermindate'  => date("Y-m-d", strtotime(date('Y-m-d').'+ 14 days'))
    ];
    $builder->update($d);
  }

 
  
}