<?php 
namespace App\Models;
use CodeIgniter\Model;
use CodeIgniter\I18n\Time;


class ProjektoModel extends Model
{
    protected $table = 'projekto';


    public function get1Projekto($id)
    {
      if ($id > 0){
        $db = db_connect();
        $strsql = "SELECT * FROM projekto where id = ".$id." ";
        $query = $this->db->query($strsql);
        $r = $query->getRow();

      }
      else
      {
        $r = new class{};
        $r->nom = '-';
        $data['r'] = $r;
      }
      return  $r;

    }
  

    public function getProjektoj($p)
    {
      $session = \Config\Services::session();

      $db = db_connect();
      $where = "where id > 0 ";
      if ($session->useradmin <> 'A')
      {
        $where .= " and uzanto = '".$p['userapp']."'";
      }

      $listestatus = array("3", "5", "7");
      if (in_array($p['status'], $listestatus)) {
         $where.= " and status = '".$p['status']."'";
      }
      $orderby = " order by nom,ordre,datreal desc, id desc";
      $strsql = "select * from projekto ".$where.$orderby;
    // echo $strsql;
      $query = $db->query($strsql);
      $d = $query->getResult();
      return  $d;
    }


    function getProjektoDropDown(){
      $session = \Config\Services::session();
       $db = db_connect();
       $strsql = "select * from projekto where status < '7' order by nom";
       $query = $db->query($strsql);
       $data['t'] = $query->getResult();
       return  $data['t'];
    }

    public function calculordre()
    {
      $db = db_connect();
      // calcul de l'ordre pour toutes les tâche non termimés
      $strsql = "select * from projekto where status < '7'";
     // echo $strsql;
      $db = db_connect();
      $query = $db->query($strsql);
      $t = $query->getResult();
      foreach ($t as $r) {
        $this->calcul1ordre($r);
      }

    }

    public function calcul1ordre($r)
    {
      $this->updProjektoFromTasko($r->id);
      // durée estimé en heures - durée réelle (à calculer des tâches) + nombre jours jusqu'à la date échéance + priorité
      $wudate = date('Y-m-d');
      $durationreal = 0;
      if ($r->durationreal > 0) $durationreal = $r->durationreal;
      $durationestim= 0;
      if ($r->durationestim > 0) $durationestim = $r->durationestim;
      // nombre de jour jusqu'à échéance : plus c'est loin dans le temps plus loin sera l'ordre

      // est ce utile ?
      $durationestim= 0;
      if ($r->durationestim > 0) $durationestim = $r->durationestim;
      $termindate=   $wudate ;
      if ($r->termindate > '') $termindate = $r->termindate;

      // si l'échéance est loin dans le future plus le nombre sera élevé
      $hodiau = Time::parse($wudate);
      $termin = Time::parse($wudate);
      if ($r->termindate > '2000-01-01') $termin    = Time::parse($r->termindate);
      $diff = $hodiau->difference($termin);

      $prio = 0;
      if ($r->priorit > 1) $prio = ($r->priorit - 50) * 2 ;
     // echo'<br/>';
     // $ordre = $tdurationestim - $tdurationreal + $diff->days + $prio;
      $ordre =  $durationestim -  $durationreal  +  $diff->days + $prio;
      $db = db_connect();
      $builder = $db->table('projekto');
      $builder->where('id', $r->id);
      $d = [
        'ordre'  => $ordre
      ];
      $builder->update($d);
    }
    
    public function updProjektoFromTasko($id)
    {
      $wudate = date('Y-m-d');
      // durée estimée et réelle = total des durées des tâches
      $strsql = "select sum(tdurationestim) as durationestim, sum(tdurationreal) as durationreal, max(ttermindate) as termindate from tasko where projektoid =".$id;
    //  echo $strsql;
      $db = db_connect();
      $query = $db->query($strsql);
      $r = $query->getRow();
     // $tdurationreal = $r->duration ;
      $builder = $db->table('projekto');
      $builder->where('id', $id);
      $d = [
        'durationestim'  => $r->durationestim,
        'durationreal'  => $r->durationreal,
        'datreal'  =>  $wudate
      ];
      $builder->update($d);
    }
    
  function CreationTable(){
    echo 'creation table';
    $db = db_connect();
    $DB_TYPE = getenv('DB_TYPE');
    if ( $DB_TYPE == 'SQLite' )
    {
      $strsql = " CREATE TABLE `projekto` (
        `id` INTEGER,
        `nom` TEXT,
        `desc` TEXT,
        `categ` TEXT,
        `uzanto` TEXT,
        `durationestim` INT,
        `durationreal` INT,
        `termindate` TEXT,
        `priorit` INT,
        `status` TEXT,
        `datcrt`	TEXT,
    	  `usrcrt`	TEXT,
    	  `datupd`	TEXT,
    	  `usrupd`	TEXT,
         datreal TEXT,
         PRIMARY KEY(`id`))";
  
        echo $strsql;
        $db->query($strsql);
      
    }

    $data['logcode'] = 'Install';
    $data['logtext'] = 'Création table projekto  '.$strsql.'  ';
    $log = new LogModel();
    $log->AddLog($data);
  }
 
}
