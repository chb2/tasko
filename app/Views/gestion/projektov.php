<div class="container">
    <div class="row">
        <div class="col-sm-1" >
        <?php 
              $urledit = site_url('/gestion/'.$dbtable.'/edit/'.$p->id);
              $urlclo = site_url('/gestion/'.$dbtable.'/cloturer/'.$p->id);
              $urlcom = site_url('/gestion/'.$dbtable.'/commencer/'.$p->id);
             ?>

            <a href="<?php echo $urledit;?>">

                  <i class="fa fa-pencil-square-o fa-3x" aria-hidden="true"></i>
                </a>  
        </div>
        <div class="col-sm-11" >
            <h1 class="titrepage"><?php echo $p->nom;?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6" >
            <p>
            <?php echo $p->desc;?>
            </p>
            <p>
            <?php
            helper('form');
            echo form_open('gestion/tasko/ajout'); 
            echo form_hidden('projektoid',$p->id);
            echo form_hidden('reiro','projekto');
            $txtbouton = "Ajout tâche";
            $classbouton = "class='btn btn-primary'";
            echo form_submit('submit',$txtbouton, $classbouton);
            echo form_close();


           ?>
            </p>
        </div>

        <div class="col-sm-6" >
            <div class="container">
 
                <div class="row">
                    <div class="col-sm-6" >
                        Catégorie
                    </div>
                    <div class="col-sm-6" >
                        <?php echo $categ; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6" >
                        Durée réelle en heures
                    </div>
                    <div class="col-sm-6" >
                        <?php echo $p->durationreal; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6" >
                        Durée estimée en heures
                    </div>
                    <div class="col-sm-6" >
                        <?php echo $p->durationestim; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6" >
                        Date échéance
                    </div>
                    <div class="col-sm-6" >
                        <?php echo $p->termindate; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6" >
                        Date réalisation
                    </div>
                    <div class="col-sm-6" >
                        <?php echo $p->datreal; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6" >
                        Etat
                    </div>
                    <div class="col-sm-6" >
                        <?php echo $statusName; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>  
 
    <?php foreach ($t as $r): ?>
        <div class="row">

            <div class="col-1  display:inline;  ">
                        <?php 
                            $urledit = site_url('/gestion/tasko/edit/'.$r->tid);
                            $urlvue = site_url('/gestion/tasko/vue/'.$r->tid);
                            $urlsup = site_url('/gestion/tasko/sup/'.$r->tid);
                            ?> 
                            <div class="" display:inline; >
                           <!--     <a href="<?php echo $urledit;?>">
                                        <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                </a> -->

                                <?php
                                    helper('form');
                                    echo form_open('gestion/tasko/edit/'.$r->tid); 
                                    echo form_hidden('projektoid',$r->projektoid);
                                    echo form_hidden('reiro','projekto');
                                    $txtbouton = 'Edit';
                                    $classbouton = "";
                                    echo form_submit('submit',$txtbouton, $classbouton);
                                    echo form_close();
                                ?>
                            </div>
            </div>
            <div class="col-11" >
                            <?php
                            $style = '';
                            if ($r->tstatus == '3')
                            {
                            $style = 'style="color:#55a; background-color:#fff; font-weight: 300; font-size : 1em "';
                            }
                            if ($r->tstatus == '7')
                            {
                                $style = 'style="color:#777; background-color:#fff; font-weight: 300; font-size : 0.9em !important;"';
                            }
                            ?>
                            <a class="klakebla listetasko" <?php echo $style; ?> href="<?php echo $urlvue;?>"><?php echo $r->tnom;?> 
                            </a>
            </div>



        </div>
    <?php endforeach ?>
        

            <p><!-- <a href="<?php echo site_url('/gestion/projekto/cloturer');?>" class="btn btn-primary">Cloturer </a></p>-->
    </div>        
</div>