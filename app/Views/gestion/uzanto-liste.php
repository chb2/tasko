<div class="container">

    <h1 class="titrepage"><?= lang('Text.uzantotitle') ?></h1>
    <p> <a href="<?php echo site_url('/gestion/'.$page.'/ajout');?>" class="btn btn-primary"><?= lang('Text.add') ?></a></p>



    <table class="table table-responsive table-striped table-bordered">
        <?php foreach ($t as $r): ?>
        <?php 
        $urledit = site_url('gestion/'.$page.'/edit/'.$r->id);
        $urlsup = site_url('gestion/'.$page.'/sup/'.$r->id);
        ?>
        <tr>
            <td>
                <a href="<?php echo $urledit;?>">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </a>  
            </td>
            <td>
                <?php echo $r->code;?>
        
            </td>
            <td>
                <?php echo $r->libelle;?>        
            </td>
            <td>
                <?php echo $r->email;?>        
            </td>

        </tr>
    <?php endforeach ?>
    </table>
</div>

 
