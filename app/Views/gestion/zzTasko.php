<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\PageModel;
use App\Models\TaskoModel;
use App\Models\ParamModel;
use App\Models\TaskerofaritaModel;
use App\Models\ProjektoModel;


class Tasko extends BaseController {
    public static  $page = 'tasko';
    public static  $table = 'tasko';

    public function __construct()
    {
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
    }

    public function index()
    {
        $this->liste('param');

    }

    public function liste($type = '5')
	{
        
        $data['view']['title'] = "Tâches";
        $data['dbtable'] = "tasko";
        $data['table'] = self::$table;
        $data['page'] = self::$page;

        $p['tstatus'] = $type;
        if ($type == '3') $p['tstatus'] = '3';
        if ($type == '7') $p['tstatus'] = '7';
        if ($type == '0') $p['tstatus'] = '0';
        $model = new TaskoModel();
        $data['t'] = $model->getTaskoj($p);
        $t = $data['t'] ;


        $page = new PageModel();
        $page->gestion($data,'tasko-liste');

      //  $page->showme('user-liste',$data);
    }

      
    function ajout(){
        $session = \Config\Services::session();
        $data['dbtable'] = "tasko";
        $data['view']['title'] = "Ajout d'une tache";
        $data['action'] = 'add';
        $date14 = date("Y-m-d", strtotime(date('Y-m-d').'+ 14 days'));
        $r = new class{};
        $r->projektoid = 0;
        $r->tnom = "";
        $r->tdesc = '';
        $r->tdurationreal = 0;
        $r->tdurationestim = 0;
        $r->tpriorit = 50;
        $r->ttermindate = date("Y-m-d", strtotime(date('Y-m-d').'+ 14 days'));
        $r->tcateg = "";
        $r->tstatus = "3";
        $data['r'] = $r;

        $projekto = new ProjektoModel();
        $data['ldprojekto'] = $projekto->getProjektoDropDown();

        $param = new ParamModel();
        $data['categs'] = $param->getparams('categ');

        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }

    function add(){
        $d = $this->addupd('a');
        $db = db_connect();
        $db->table('tasko')->insert($d);
        $this->liste();
	}

    function edit($id){
        $data['dbtable'] = "tasko";
        $data['page'] = self::$page;
        $data['view']['title'] = "Modification d'une tache";
        $data['action'] = 'upd';
        $strsql = "select * from tasko where tid = $id";
        $db = db_connect();
        $query = $db->query($strsql);
        $data['r'] = $query->getRowArray();


        $tasko = new TaskoModel();
        $data['r'] = $tasko->get1tasko($id);


        $projekto = new ProjektoModel();
        $data['ldprojekto'] = $projekto->getProjektoDropDown();


        $model = new TaskerofaritaModel();
        $data['nbtf'] = $model->getNbTaskerofaritaj($id);

        $param = new ParamModel();
        $data['categs'] = $param->getparams('categ');


     //   $page = new Page();
     //   $page->showme('tasko-edit',$data);

        $page = new PageModel();
        $page->gestion($data,'tasko-edit');
   
    }

    public function upd() {


        helper(['form', 'url']);
        $validation =  \Config\Services::validation();
        $validation->setRule('tnom', 'tnom', 'required');

        if (! $this->validate([
            'tnom' => "required"]))
        {
          //echo 'pas bon ';
           $errors = $validation->getErrors();
          // var_dump($errors);
           //$this->edit($id);
           $data['dbtable'] = "tasko";
           $data['view']['title'] = "Modification d'une tache";
           $data['errors'] = 'Saisir une tâche';
           $data['action'] = 'upd';
           $id = $this->request->getVar('tid');
           $strsql = "select * from tasko where tid = $id";
         //  echo $strsql;
           $db = db_connect();
           $query = $db->query($strsql);
           $data['r'] = $query->getRowArray();

   
            $page = new Page();
           $page->showme('tasko-edit',$data);
        }
        else
        {
        //    echo 'bon';
 

        $date = date('Y-m-d');
 
        $id = $this->request->getVar('tid');
        $session = \Config\Services::session();
        $d = [
            'tnom'  => $this->request->getVar('tnom'),
            'tdesc'  => $this->request->getVar('tdesc'),
            'tdurationestim'  => $this->request->getVar('tdurationestim'),
            'ttermindate'  => $this->request->getVar('ttermindate'),
           
            'tstatus'  => $this->request->getVar('tstatus'),
            'tdatupd' =>  $date ,
            'tusrupd' => $session->userapp
        ];
        $d = $this->addupd('u');
        $db = db_connect();
        $builder = $db->table('tasko');
        $builder->where('tid', $id);
        $builder->update($d);


        $this->liste(); 
        }
    }



    function addupd($t){
        $wudate = date('Y-m-d');
        $session = \Config\Services::session();
        $d['projektoid'] = $this->request->getVar('projektoid');

        $d['tnom'] = $this->request->getVar('tnom');
        $d['tdesc'] = $this->request->getVar('tdesc');
        $d['tpriorit'] = $this->request->getVar('tpriorit');
        $d['tdurationestim'] = $this->request->getVar('tdurationestim');
        $d['ttermindate'] = $this->request->getVar('ttermindate');
        $d['tstatus'] = $this->request->getVar('tstatus');
        $d['tcateg'] = $this->request->getVar('tcateg');
        $d['tdatupd'] = $wudate;
        $d['tusrupd'] = $session->userapp;
        if ($t=="a"){
            $d['tdatcrt'] = $wudate;
            $d['tusrcrt'] =  $session->userapp;
            $d['tuzanto'] =  $session->userapp;
            }
        return $d;
        }

        public function vue($id)
        {
            $data['dbtable'] = "tasko";
            $data['view']['title'] = "Réalisation d'une tache";

            $model = new TaskoModel();
            $data['t'] = $model->get1tasko($id);
        
            $session = \Config\Services::session();
            $p['userapp'] = $session->userapp;
            $p['useradmin'] = $session->useradmin;
            $p['tid'] = $id;
            $model = new TaskerofaritaModel();
            $data['tf'] = $model->getTaskerofaritaj($p);
        

            $page = new PageModel();
            $page->gestion($data,'tasko-tf');
        }
        public function addtf()
        {
            $session = \Config\Services::session();
            $tfduration =1;
            if ($this->request->getVar('tfduration') > 0){$tfduration = $this->request->getVar('tfduration');}
            $id = $this->request->getPostGet('tftid');
            $wudate = date('Y-m-d');
            $d = [
                'tftid'  => $this->request->getPostGet('tftid'),
                'tfuzanto' => $session->userapp,
                'tfdate'  => $this->request->getVar('tfdate'),    
                'tfdesc'  => $this->request->getVar('tfdesc'),    
                'tfduration'  => $tfduration,      
                'tfdatcrt' =>  $wudate ,
                'tfusrcrt' => $session->userapp,
                'tfdatupd' =>  $wudate ,
                'tfusrupd' => $session->userapp
            ];
           // var_dump($d);
            
            $db = db_connect();
            $db->table('taskerofarita')->insert($d);
            // mise à jour de la table tasko
        
            // calul de la durée total réalisé et date de dernière tache réalisée
            $model = new TaskoModel();
            $model->updDurationreal($id);
            $model->updDateReal($id,$wudate);  
        
            $this->vue($id);
        
        }

    function calculordre(){
        $tasko = new TaskoModel();
        $tasko->calculordre();
        $this->liste(); 

       /* $db = db_connect();

        $this->liste();     */           
    }
        function sup($id){

             $data['dbtable'] = "tasko";
            $data['page'] = self::$page;
           // $data['view']['title'] = "Modification d'une tache";
            $data['view']['title'] = "Suppression";
            //$data['action'] = 'upd';
            $data['action'] = 'del';
            $strsql = "select * from tasko where tid = $id";
            $db = db_connect();
            $query = $db->query($strsql);
            $data['r'] = $query->getRowArray();
    
            $projekto = new ProjektoModel();
            $data['ldprojekto'] = $projekto->getProjektoDropDown();
    
            $tasko = new TaskoModel();
            $data['r'] = $tasko->get1tasko($id);
    
    
            $model = new TaskerofaritaModel();
            $data['nbtf'] = $model->getNbTaskerofaritaj($id);
            $param = new ParamModel();
            $data['categs'] = $param->getparams('categ');
        
        
            $page = new PageModel();
            $page->gestion($data,self::$page.'-edit');
        
        
        }

        /* ===== suppression reel ===== */
        function del(){

                    $id = $this->request->getVar('id');
                    $db = db_connect();
                    $builder = $db->table(self::$table);
                    $builder->where('tid', $id);
                    $builder->delete();
                    $this->liste();
        
        }
        /* ===== suppression reel ===== */
    function del2($id){
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('tid', $id);
        $builder->delete();
        $this->liste();                
    }

    function updtab1(){
        $tasko = new TaskoModel();

        $tasko->addfield('projektoid','INTEGER');
        $tasko->addfield('ordre','INTEGER');
              
    }

}