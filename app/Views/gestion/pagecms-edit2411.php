<?php
$session = \Config\Services::session();
?>
<div class="container">
<h1 class="titrepage"><?php echo $view['title'] ;?></h1>
<?php
//
//$page="note";

//echo "page".$page;
helper('form');



if ($action == "add"){ 
    echo form_open('gestion/'.$page.'/add'); 
    $txtbouton = "Ajout";
    $classbouton = "class='btn btn-primary'";
}
if ($action == "upd"){ 
    echo form_open('gestion/'.$page.'/upd'); 
    $txtbouton = "Mise à jour";
    $classbouton = "class='btn btn-primary'";
}
if ($action == "del"){
     echo form_open('gestion/'.$page.'/del'); 
     $txtbouton = "Suppression"; 
     $classbouton = "class='btn btn-danger'";}

?>
<?php
  if (!empty($erreurs)) {
    echo "<br/>";
    ?>
  <div class="alert alert-danger" role="alert">
 <p>Attention il y a au moins une erreur</p>
    <?php foreach ($erreurs as $erreur): ?>
        <li><?php echo $erreur; ?></li>
        <?php endforeach ?>
  </div>
  <?php } ?>

  <div class="form-group row">
    <span class="col-sm-2 col-form-label" >Code</span>
        <div class="col-10">
        <?php


            $data = array(
                        'name'        => 'code',
                        'type'        => 'text',
                        'value'       =>  $r->code,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

  <div class="form-group row">
    <span class="col-sm-2 col-form-label" >Titre</span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'titre',
                        'type'        => 'text',
                        'value'       =>  $r->titre,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" >Texte court</span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'textecourt',
                        'type'        => 'text',
                        'value'       =>  $r->textecourt,
                        'style'       => 'width: 100%; height:3em;'
                        );
            echo form_textarea($data);
            ?>
        </div>
    </div>

  <div class="form-group row">
    <span class="col-sm-2 col-form-label" >Texte</span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'textelong',
                        'type'        => 'text',
                        'value'       =>  $r->textelong,
                        'style'       => 'width: 100%'
                        );
            echo form_textarea($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" >Metatitle</span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'metatitle',
                        'type'        => 'text',
                        'value'       =>  $r->metatitle,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" >Metadescription</span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'metadescription',
                        'type'        => 'text',
                        'value'       =>  $r->metadescription,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" >Ordre</span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'ordre',
                        'type'        => 'text',
                        'value'       =>  $r->ordre,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>


    <div class="form-group row">
    <span class="col-sm-2 col-form-label" >visible</span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'visible',
                        'type'        => 'text',
                        'value'       =>  $r->visible,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>
    <div class="form-group row">
    <span class="col-sm-2 col-form-label" >Début validité</span>
        <div class="col-10">
        <?php
                 $data = array(
                    'name'        => 'datdeb',
                    'type'        => 'date',
                    'value'       => $r->datdeb,
                    'style'       => 'width: 10em;'
                    );
        echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" >Début validité</span>
        <div class="col-10">
        <?php
                 $data = array(
                    'name'        => 'datfin',
                    'type'        => 'date',
                    'value'       => $r->datfin,
                    'style'       => 'width: 10em;'
                    );
        echo form_input($data);
            ?>
        </div>
    </div>

    <?php
    if ($action != "add"){
    ?>
    <div class="form-group row">
    <span class="col-sm-2 text-secondary">Création </span>
    <span class="col-sm-2 text-secondary"><?php echo $r->datcrt;?> </span>
    <span class="col-sm-2 text-secondary"> <?php echo $r->usrcrt;?> </span>
</div>
<div class="row">
    <span class="col-sm-2 text-secondary">Modification </span>
    <span class="col-sm-2 text-secondary"><?php echo $r->datmod;?></span>
    <span class="col-sm-2 text-secondary"> <?php echo $r->usrmod;?> </span>
</div>
<?php
    }
    ?>

<div class="form-group row">
    <div class="col-sm-2">
    <?php
    if ($action <> "add"){
    echo form_hidden('id',$r->id);}

    if ($action <> 'vis'){
    echo form_submit('submit',$txtbouton, $classbouton);
    }
    echo form_close();
    ?>
    </div>
</div>
</div>