<?php
  //    $language = \Config\Services::language();
  //    $language->setLocale('en'); 
     // $locale = $this->request->getLocale();
    //  echo $this->request->getLocale();
?>
<div class="container">

  <div class="row">
    <div class="col-sm-4" >

    </div>
    
    <div class="col-sm-4 grey-border arrondi" >
      <h1 class="titrepage"><?= lang('Text.login') ?></h1>
      <?php
      if ($msgerr > ''){
        echo '<br/><p class="alert alert-danger">'.$msgerr.' </p>';
      }
      ?>

      <?php
      helper('form');

      $url = base_url('index.php/gestion/login/logincheck');
      echo form_open($url);
      echo "<label for='p'>".lang('Text.userid')."<br/>";
      $data = array(
          'name'        => 'u',
          'style'     => 'width: 100%'
        );
      echo form_input($data);

      ?>
    

      <?php
      echo "<label for='p'>".lang('Text.password')."</label><br/>";
      $data = array(
        'name'      => 'p',
        'style'     => 'width: 100%'
        

      );
      echo form_password($data);
      echo "</p>";
      $classbouton = "class='btn-block btn-primary'";
      echo form_submit('submit',lang('Text.ok'),$classbouton);
      echo form_close();
      ?>
 <br/>
      <div>

          <p> <a href="<?php echo base_url('index.php/gestion/login/demchgpas1');?>" ><?=lang('Text.lostpassword')?></a></p>
      </div>
      <br/>

    </div>
    <div class="col-sm-4" >
    
    
    </div>
  </div>
  
</div>
  