<div class="container">
<h1 class="titrepage"><?php echo $view['title'] ;?></h1>
<p> <a href="<?php echo site_url('/gestion/'.$page.'/ajout');?>" class="btn btn-primary">Ajout</a></p>

    <table class="table table-responsive table-striped table-bordered">
        <tr>
            <th></th>
            <th>Sprint</th>
            <th>But</th>
            <th>Début</th>
            <th>Fin</th>
        </tr>
        <?php foreach ($t as $r): ?>
            <?php 
            $urledit = site_url('/gestion/'.$page.'/edit/'.$r->id);
            $urlvue = site_url('/gestion/'.$page.'/vue/'.$r->id);
            $urlsup = site_url('/gestion/'.$page.'/sup/'.$r->id);
            ?>
        <tr>
            <td>
                  <a href="<?php echo $urledit;?>">
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                  </a>  
            </td>
            <td>
                <span>      
                    <a class="klakebla listetasko" href="<?php echo $urlvue;?>">
                        <?php echo $r->nom;?>               
                    </a>
               </span>
            </td>
            <td>
                <?php
                echo $r->goal; 
                ?>
            </td>
            <td>
                <?php
                echo $r->datdeb; 
                ?>
            </td>
            <td>
                <?php
                echo $r->datfin; 
                ?>
            </td>            

 
 



        </tr>
        <?php endforeach ?>
    </table>
</div>
