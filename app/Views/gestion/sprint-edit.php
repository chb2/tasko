<div class="container">
    <h1 class="titrepage"><?php echo $view['title'] ;?></h1>

    <?php
    helper('form');

    if ($action == "add"){ echo form_open('gestion/'.$page.'/add'); $txtbouton = "Ajout";}
    if ($action == "upd"){ echo form_open('gestion/'.$page.'/upd'); $txtbouton = "Mise à jour";}
    if ($action == "del"){ echo form_open('gestion/'.$page.'/del'); $txtbouton = "Suppression";}

    if ($action == "add"){ 
        echo form_open($page.'/add'); 
        $txtbouton = "Ajout";
        $classbouton = "class='btn btn-primary'";
    }
    if ($action == "upd"){ 
        echo form_open($page.'/upd'); 
        $txtbouton = "Mise à jour";
        $classbouton = "class='btn btn-primary'";
    }
    if ($action == "del"){
        echo form_open($page.'/del'); 
        $txtbouton = "Suppression"; 
        $classbouton = "class='btn btn-danger'";}
    ?>

<?php
  if (!empty($erreurs)) {
    echo "<br/>";
    ?>
  <div class="alert alert-danger" role="alert">
 <p>Attention il y a au moins une erreur</p>
    <?php foreach ($erreurs as $erreur): ?>
        <li><?php echo $erreur; ?></li>
        <?php endforeach ?>
  </div>
  <?php } ?>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Nom</span>
        <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'nom',
                        'type'        => 'text',
                        'value'       => $r->nom,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Objectif</span>
        <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'goal',
                        'type'        => 'text',
                        'value'       => $r->goal,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Début</span>
        <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'datdeb',
                        'type'        => 'date',
                        'value'       => $r->datdeb,
                        'style'       => 'form-control'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Fin</span>
        <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'datfin',
                        'type'        => 'date',  
                        'value'       => $r->datfin,
                         'style'       => 'form-control'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Nombre d'heures maximum</span>
        <div class="col-10">
            <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'nb_h_max',
                        'type'        => 'string',  
                        'value'       => $r->nb_h_max,
                        'style'       => 'width: 3em'
                        );
            echo form_input($data);
            ?>
        </div>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Description</span>
        <div class="col-10">
        <?php
        $data = array(
                    'name'        => 'desc',
                    'type'        => 'text',
                    'id'          => 'textareatinymce',
                    'value'       => $r->desc,
                    'style'       => 'width: 100%'
                    );
        echo form_textarea($data);
        ?>
        </div>
    </div>

  

 
    <?php
    if ($action != "add"){
    ?>


        <div class="form-group row">
            <span class="col-sm-2 text-secondary">Création </span>
            <span class="col-sm-2 text-secondary"><?php echo $r->datcrt;?> </span>
            <span class="col-sm-2 text-secondary"> <?php echo $r->usrcrt;?> </span>
        </div>
        <div class="row">
            <span class="col-sm-2 text-secondary">Modification </span>
            <span class="col-sm-2 text-secondary"><?php echo $r->datupd;?></span>
            <span class="col-sm-2 text-secondary"> <?php echo $r->usrupd;?> </span>
        </div>
        <?php
        if (isset($r->uzanto)){
        echo form_hidden('uzanto',$r->uzanto);
        }
    }
    ?>

    <div class="form-group row">
    <div class="col-10"></div>
    <div class="col-2">
        <?php

        if ($action <> "add"){
        echo form_hidden('id',$r->id);}

        if ($action <> 'vis'){
        echo form_submit('submit',$txtbouton, $classbouton);
        }

        echo form_close();
        ?>
        </div>
    </div>
</div>
