<?php
if (!isset($classbouton)) $classbouton = "btn-block btn-primary";
if (!isset($classalert)) $classalert = "alert alert-danger";
?>
<div class="container">
  <div class="row">
    <div class="col-sm-2" >
    </div>
    <div class="col-sm-6 grey-border arrondi" >
      <h1 class="titrepage"><?php echo $view['title'] ;?></h1>
      <?php
     if ($msgerr > ''){
         echo '<br/><p class="'.$classalert.'">'.$msgerr.' </p>';
      }
      ?>
      <?php
      helper('form');
      echo form_open("gestion/utileco/".$action);
      echo form_submit('submit',$txtbouton,"class='".$classbouton."'");
      echo form_close();
      ?>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12" >
      <br/>
    </div> 
  </div>
</div>  
