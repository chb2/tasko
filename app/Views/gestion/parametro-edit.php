<div class="container">
<h1 class="titrepage"><?php echo $view['title'] ;?></h1>
<p>
<?php
//echo 'Type de champ : '.$type_field_param;
?>

</p>

<?php
helper('form');
echo form_open_multipart('gestion/parametro/updtyco'); 
$txtbouton = "Mise à jour";
$classbouton = "class='btn btn-primary'";

       

?>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Type</span>
        <div class="col-10">
            <?php
            echo form_hidden('paty',$r->paty);
            echo $r->paty;
            ?>
        </div>
    </div>
    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Code</span>
        <div class="col-8">
            <?php
            echo form_hidden('paco',$r->paco);
            echo $r->paco;
            ?>
        </div>
    </div>
    <?php
    if ($type_field_param == 'dropdown')
    {
        ?>
        <div class="form-group row">
            <span class="col-sm-2 col-form-label" >Valeur</span>
            <div class="col-8">
                <select name="zona" class="form-control">
                <?php 
                    foreach($pagecmss as $pagecms)
                    { 
                        $selected ='';
                        if ($r->zona == $pagecms->id) $selected = "selected";
                    echo '<option value="'.$pagecms->id.'" '.$selected.'>'.$pagecms->code.'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
    <?php
    }
    ?>
    <?php
    if ($type_field_param == 'edit')
    {
    ?>
    

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Valeur</span>
        <div class="col-8">
        <?php

            $data = array(
                        'name'        => 'zona',
                        'type'        => 'text',
                        'value'       =>  $r->zona,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>
    <?php
    }
    ?>


    <div class="form-group row">
    <div class="col-sm-2">
    <?php

    echo form_submit('submit',$txtbouton, $classbouton);

    echo form_close();
    ?>
    </div>
</div>
</div>