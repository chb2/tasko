<div class="container">
    <h1 class="titrepage"><?php echo $view['title'] ;?></h1>
    <?php $urlajout = '/gestion/'.$page.'/ajout/'.$paty;
   // echo $urlajout;?>
    <p> <a href="<?php echo site_url($urlajout);?>" class="btn btn-primary">Ajout</a></p>



    <table class="table table-responsive table-striped table-bordered">
        <?php foreach ($t as $r): ?>
        <?php 
        $urledit = site_url('gestion/'.$page.'/edit/'.$r->id);
        $urlsup = site_url('gestion/'.$page.'/sup/'.$r->id);
        $urldet = site_url('gestion/'.$page.'/liste/'.$r->paco);
        ?>
        <tr>
            <td>
                <a href="<?php echo $urledit;?>">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </a>  
            </td>

            <?php 
            if ($r->paty =='PARAM' or $r->paty =='param')
            {
            ?>
            <td>
                <a href="<?php echo $urldet;?>">
                detail
                </a>  
            </td>
            <?php 
            }
            ?>
            <td>
                <?php echo $r->paco;?>        
            </td>
            <td>
                <?php echo $r->libelle;?>        
            </td>
            <td>     
                <?php
                    echo "<a href=".$urlsup."><i class='fa fa-times text-danger' aria-hidden='true'></i></a>";
                ?>
            </td>

        </tr>
    <?php endforeach ?>
    </table>
</div>

 
