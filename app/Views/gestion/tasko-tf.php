<div class="container">
  <div class="row">
    <div class="col-sm-1" >
    <?php 
              $urledit = site_url('/gestion/'.$dbtable.'/edit/'.$t->tid);
              $urlclo = site_url('/gestion/'.$dbtable.'/cloturer/'.$t->tid);
              $urlcom = site_url('/gestion/'.$dbtable.'/commencer/'.$t->tid);
             ?>

            <a href="<?php echo $urledit;?>">

                  <i class="fa fa-pencil-square-o fa-3x" aria-hidden="true"></i>
                </a>  
    </div>
    <div class="col-sm-11" >
     <h1 class="titrepage"><?php echo $t->tnom;//echo $view['title'] ;?></h1>
    </div>
  </div>

    <div class="row grey-border bg-grey">
        <div class="col-sm-8" >
        <p class="titredesctache"><?php echo $t->tdesc; ?> </p>
        </div>
        <div class="col-sm-4" >
          <div class="container">
            <div class="row">
                <div class="col-sm-6" >
           
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6" >
                Projet
              </div>
              <div class="col-sm-6" >
              <?php 
                  //  $urlsup = site_url('/gestion/'.$page.'/sup/'.$r->id);
                $urlvue_projekto = site_url('/gestion/projekto/vue/'.$t->projektoid);
                echo "<a href=".$urlvue_projekto.">".$projektoNom."</a>"; 
              ?>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6" >
                Catégorie
              </div>
              <div class="col-sm-6" >
              <?php echo $categ; ?>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6" >
             Heures 
              </div>
               <div class="col-sm-6" >
              <?php echo $t->tdurationreal; ?>
              /
              <?php echo $t->tdurationestim; ?>   
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6" >
              Date échéance
              </div>
              <div class="col-sm-6" >
                <?php echo $t->ttermindate; ?>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6" >
              Date réalisation
              </div>
              <div class="col-sm-6" >
                <?php echo $t->tdatreal; ?>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6" >
              Etat
              </div>
              <div class="col-sm-6" >
                <?php echo $t->tstatus.' '.$statusName; ?>
              </div>
            </div>

          </div>

        </div>
    </div>

    <div class="form-group row grey-border bg-grey form-grid">
        <div class="col-sm-12" >
        <?php
        helper('form');
        echo form_open('gestion/tasko/addtf');


        $data = array(
            'name'        => 'tfduration',
            'style'       => 'width:10%'
        );
        echo form_input($data);
        echo 'h';
        $data = array(
        'name'        => 'tfdesc',
        'style'       => 'width:70%;margin-left:10px;'
        );
        echo form_input($data);

        $wudate = date('Y-m-d');
        echo form_hidden('tfdate',$wudate);
        echo form_hidden('tftid',$t->tid);
        $classbouton = "class='btn btn-primary'";
        echo form_submit('submit','Ajout réalisation', $classbouton);
        echo form_close();
        $totalduration = 0;
        ?>
    
        </div>
 
    </div>
    <br/>
    <div class="row">
      <div class="col-sm-12" >
      <table class="table table-responsive table-striped table-bordered">
      <?php  foreach ($tf as $r): ?>
        <tr>
         <td>
            <?php echo $r->tfdate;?>
          </td>
          <td>
            <?php echo $r->tfdesc;?>
          </td>
          <td>
            <?php echo $r->tfduration;
            $totalduration = $totalduration + $r->tfduration;
            ?>
          </td>
        </tr>
      <?php endforeach ?>
        <tr>
          <td></td>
          <td>Total</td>
          <td><?php echo $totalduration;?></td>
        </tr>
      </table>

    </div>
    <div class="col-sm-12" >
      <?php
      if ($t->tstatus =='5'){
      ?>
    <p> <a href="<?php echo $urlclo;?>" class="btn btn-primary">Cloturer</a></p>
    <?php
     }
      ?>

<?php
      if ($t->tstatus < '5'){
      ?>
    <p> <a href="<?php echo $urlcom;?>" class="btn btn-primary">Commencer</a></p>
    <?php
     }
      ?>

    </div>
  
  </div>


</div>