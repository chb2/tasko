<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <?php
        $metatitle = AA_APPLICATION  ;
        if (isset($meta['title'])) $metatitle = $meta['title'];
        $metadescription = "";
        if (isset($meta['description'])) $metadescription = $meta['description'];
        ?>
        <title><?php echo $metatitle; ?></title>
        <meta name="description" content="<?php echo $metadescription; ?>" />

        
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css');?>">
      <link rel="stylesheet" href="<?php echo base_url('../fa/css/fork-awesome.min.css');?>">
      <link rel="stylesheet" href="../fa/css/fork-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url('css/style.css?v0.6.1');?>">
         <!-- tinymce debut -->
         <script src="<?php echo base_url('js/tinymce/tinymce.min.js');?>" referrerpolicy="origin"></script>
         <script src="<?php echo base_url('js/tool/js.js');?>" referrerpolicy="origin"></script>

        <script>
      tinymce.init({
        selector: '#textareatinymce',
        plugins: 'link image code',
        toolbar: 'undo redo image link code'
      });
    </script>

         <!-- tinymce fin -->

        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    </head>
 
    <body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container">
                <a class="navbar-brand" href="<?php echo site_url('');?>"><?php echo AA_APPLICATION ?></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon ml-auto"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo site_url('');?>">
                                Accueil
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="<?php echo site_url('gestion/taskerofarita');?>">Réalisations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="<?php echo site_url('gestion/sprint');?>">Sprints</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="<?php echo site_url('gestion/projekto/liste/5');?>">Projets</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo site_url('gestion/tasko/liste/5');?>">Tâches</a>
                         </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            configuration</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo site_url('gestion/param');?>">Paramètres</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo site_url('gestion/pagecms');?>">Page</a>
                                <a class="dropdown-item" href="<?php echo site_url('gestion/uzanto');?>">Utilisateurs</a>
                                <a class="dropdown-item" href="<?php echo base_url('index.php/gestion/utileco');?>">Outils</a>
                                <a class="dropdown-item" href="<?php echo site_url('gestion/log');?>">Log</a>

                            </div>
                        </li>
                        <li><a class="nav-link" href="<?php echo site_url('gestion/login/logout');?>">Deconnexion</a></li>         
         
                    </ul>
                </div>
            </div>
        </nav>