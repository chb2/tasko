<div class="container">
    <h1 class="titrepage"><?= lang('Text.parameters') ?></h1>

    <div class="row mx-1">

        <div class="col-1" >
            <?php
            $urledit = site_url('gestion/'.$page.'/edityco/site_defaultpage');
            ?>
            <a href="<?php echo $urledit;?>">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </a>    
        </div>
        <div class="col-6" ><?= lang('Text.defaultpage') ?>
        </div>

        <div class="col-5" >
            <?php
            echo $site_defaultpage;
        

            ?>
        </div>

    </div>
    <div class="row mx-1">
        <div class="col-1" >
            <?php
            $urledit = site_url('gestion/'.$page.'/edityco/site_komputo');
            ?>
            <a href="<?php echo $urledit;?>">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </a>
        </div>

        <div class="col-6" ><?= lang('Text.counter') ?> (0/1)
        </div>


        <div class="col-5" >
            <?php
            echo $site_komputo;
        

            ?>
        </div>

    </div>



    <div class="row mx-1">
        <div class="col-12" >
        <a  href="<?php echo base_url('index.php/gestion/param');?>"><?= lang('Text.advancedsettings') ?></a>
        </div>
         
    </div>
   
</div>