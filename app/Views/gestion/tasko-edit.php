


<?php
        if (!isset($reiro)) $reiro = '';
        if (!isset($projektoid)) $projektoid = 0;
 

?>
<div class="container">
    <h1 class="titrepage"><?php echo $view['title'] ;?></h1>
    <?php //echo "nombre ".$nbtf ;?>
    <?php
    helper('form');

    if ($action == "add"){ echo form_open('gestion/tasko/add'); $txtbouton = "Ajout";}
    if ($action == "upd"){ echo form_open('gestion/tasko/upd'); $txtbouton = "Mise à jour";}
    if ($action == "del"){ echo form_open('gestion/tasko/del'); $txtbouton = "Suppression";}

    if ($action == "add"){ 
        echo form_open($dbtable.'/add'); 
        $txtbouton = "Ajout";
        $classbouton = "class='btn btn-primary'";
    }
    if ($action == "upd"){ 
        echo form_open($dbtable.'/upd'); 
        $txtbouton = "Mise à jour";
        $classbouton = "class='btn btn-primary'";
    }
    if ($action == "del"){
        echo form_open($dbtable.'/del'); 
        $txtbouton = "Suppression"; 
        $classbouton = "class='btn btn-danger'";}
    ?>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Tâche</span>
        <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'tnom',
                        'type'        => 'text',
                        'value'       => $r->tnom,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Description</span>
        <div class="col-10">
        <?php
    //  if ($action == "add"){ $tfdate =  date('Y-m-d');} else{$tfdate = $r['tfdate'];}
        $data = array(
                    'name'        => 'tdesc',
                    'type'        => 'textareatinymce',
                    'value'       => $r->tdesc,
                    'style'       => 'width: 100%'
                    );
        echo form_textarea($data);
        ?>
        </div>
    </div>

    <div class="form-group row">
        <span for="tnom" class="col-sm-2 col-form-label">Projet

        </span>

        <div class="col-10">
            <select name="projektoid" for="projektoid" class="form-control">
                    <?php
                    echo '<option value=0> - </option>';

                    foreach($ldprojekto as $row)
                    { 
                        $selected ='';
                        if ($r->projektoid == $row->id) $selected = "selected";
                    echo '<option value="'.$row->id.'" '.$selected.'>'.$row->nom.'</option>';
                    }
                    ?>
            </select>
        </div>
    </div>



    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Catégorie</span>
        <div class="col-4">
            <select name="tcateg" class="form-control">
                <?php 
                foreach($categs as $row)
                { 
                    $selected ='';
                    if ($r->tcateg == $row->paco) $selected = "selected";
                echo '<option value="'.$row->paco.'" '.$selected.'>'.$row->libelle.'</option>';
                }
                ?>
            </select>
        </div>
    </div>


    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Sprint</span>
        <div class="col-4">
            <select name="sprintid" class="form-control">
                <?php 
                echo '<option value="0">à définir</option>';
                foreach($ldsprint as $row)
                { 
                    $selected ='';
                    if ($r->sprintid == $row->id) $selected = "selected";
                echo '<option value="'.$row->id.'" '.$selected.'>'.$row->nom.'</option>';
                }
                ?>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Priorité (1-99)<br/><i>10 prioritaire 90 non prioritaire</i></span>
        <div class="col-sm-1">
        <?php
        $data = array(
            'name'        => 'tpriorit',
            'type'        => 'text',
            'id'          => 'tpriorit',
            'value'       => $r->tpriorit,
            'style'       => 'width: 100%'
         );
        echo form_input($data);
        ?>
        </div>
        <div class="col-sm-1">
       ordre
        </div>
        <div class="col-sm-1">
        <?php echo $r->ordre;?>
        </div>
    </div>


    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Durée réelle en heure</span>
        <div class="col-10">
            <?php echo $r->tdurationreal;?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Durée estimée en heures</span>
        <div class="col-2">
        <?php
        $data = array(
            'name'        => 'tdurationestim',
            'type'        => 'text',
            'id'          => 'tdurationestim',
            'value'       => $r->tdurationestim,
            'style'       => 'width: 100%'
       );
        echo form_input($data);
        ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Date échéance</span>
        <div class="col-sm-3">
            <?php
            $data = array(
                'name'        => 'ttermindate',
                'type'        => 'date',
                'id'          => 'ttermindate',
                'value'       => $r->ttermindate,
                'style'       => 'form-control'
        );
            echo form_input($data);
            ?>
        </div>
        <div class=" form-check form-check-inline">        
            <input class="form-check-input"  style="height:1.5em; width:1.5em;" type="radio" id="dp1" name="nbj"  onchange="dateplusday('ttermindate',7);" >
            <label  class="form-check-label">7 jours</label>
        </div>
        <div class="form-check form-check-inline"> 
            <input class="form-check-input" style="height:1.5em; width:1.5em;" type="radio" id="dp2" name="nbj"   onchange="dateplusday('ttermindate',30);">
            <label  class="form-check-label" for="css">1 mois</label>
        </div>
        <div class="form-check form-check-inline"> 
            <input class="form-check-input" style="height:1.5em; width:1.5em;" type="radio" id="dp3" name="nbj"  onchange="dateplusday('ttermindate',98);">
            <label  class="form-check-label" for="javascript">3 mois</label>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Etat</span>
        <div class="col-10">
            <select name="tstatus">
            <?php
            if ($action == "add") 
            {
                $tstat = '3';
            }
            else
            {
                $tstat = $r->tstatus;
            }
            // 3 à venir  5 en cours et 7 cloturé
            $seladmin="";
            $sel3 = "";
            $sel5 = "";
            $sel7 = "";
            if ($tstat == "3"){$sel3 = "selected";}
            if ($tstat == "5"){$sel5 = "selected";}
            if ($tstat == "7"){$sel7 = "selected";}
            ?>
            <option value="3" <?php echo $sel3; ?>>A venir</a>
            <option value="5" <?php echo $sel5; ?>>En cours</a>
            <option value="7" <?php echo $sel7; ?>>Cloturé</a>
            </select>
        </div>
    </div>

    <?php
    if ($action != "add"){
    ?>
        <div class="form-group row">
            <span class="col-sm-2 text-secondary">Propriétaire</span>
            <span class="col-sm-2 text-secondary"> <?php if (isset($r->tuzanto)){echo $r->tuzanto;}?></span>
            <span class="col-sm-2 text-secondary"> <?php echo $r->tdatreal;?> </span>
        </div>

        <div class="form-group row">
            <span class="col-sm-2 text-secondary">Création </span>
            <span class="col-sm-2 text-secondary"><?php echo $r->tdatcrt;?> </span>
            <span class="col-sm-2 text-secondary"> <?php echo $r->tusrcrt;?> </span>
        </div>
        <div class="row">
            <span class="col-sm-2 text-secondary">Modification </span>
            <span class="col-sm-2 text-secondary"><?php echo $r->tdatupd;?></span>
            <span class="col-sm-2 text-secondary"> <?php echo $r->tusrupd;?> </span>
        </div>
        <?php
        if (isset($r->tuzanto)){
        echo form_hidden('tuzanto',$r->tuzanto);
        }
    }
    ?>

    <div class="form-group row">
    <div class="col-10"></div>
    <div class="col-2">
        <?php
        echo form_hidden('reiro',$reiro);
        if ($action <> "add"){
        echo form_hidden('tid',$r->tid);
        }

        if ($action <> 'vis'){
        echo form_submit('submit',$txtbouton, $classbouton);
        }

        echo form_close();
        ?>
        </div>
    </div>
</div>