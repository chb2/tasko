<?php
    //echo $page;
?>
<div class="container">
    <h1 class="titrepage"><?php echo $view['title'] ;?></h1>
    <?php
    helper('form');
   
    if ($action == "add"){ 
        echo form_open('gestion/'.$page.'/add');
        $txtbouton = "Ajout";
        $classbouton = "class='btn btn-primary'";
    }
    if ($action == "upd"){ 
        echo form_open('gestion/'.$page.'/upd'); 
        $txtbouton = "Mise à jour";
        $classbouton = "class='btn btn-primary'";
    }
    if ($action == "del"){
        echo form_open('gestion/'.$page.'/del'); 
        $txtbouton = "Suppression"; 
        $classbouton = "class='btn btn-danger'";}
    ?>

    <div class="form-group row">

        <span class="col-sm-2 col-form-label" >Date</span>

        <div class="col-10">
        <?php
        if ($action == "add"){ $tfdate =  date('Y-m-d');} else{$tfdate = $r->tfdate;}
        $data = array(
                    'name'        => 'tfdate',
                    'type'        => 'date',
                    'value'       => $tfdate,
                    'style'       => 'form-control'
                    );
        echo form_input($data);
        ?>
        </div>
    </div>

    <div class="form-group row">
    <span for="tnom" class="col-sm-2 col-form-label">Tâches</span>

    <div class="col-10">
    <select name="tftid" for="tftid" class="form-control">
            <?php 

            foreach($ldtasko as $row)
            { 
                $selected ='';
                if ($r->tftid == $row->tid) $selected = "selected";
              echo '<option value="'.$row->tid.'" '.$selected.'>'.$row->tnom.'</option>';
            }
            ?>
    </select>
    </div>
</div>

<div class="form-group row">
    <span class="col-sm-2 col-form-label">Description</span>
    <div class="col-10">
    <?php
    $data = array(
                'name'        => 'tfdesc',
                'value'       => $r->tfdesc,
                'style'       => 'width:100%',         
                );
    echo form_input($data);
    ?>
    </div>
</div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label">Durée</span>
        <div class="col-10">
            <select name="tfduration" for="tfduration" class="form-control">
                        <?php 
            $duree = array(0.25,0.5, 1,1.5,2,3,4,5,6,7,8,10,12);
            $nb = count($duree);
            for($i = 0; $i < $nb; $i++){
                //echo $duree[$i]. ', ';
                $selected = " ";
                if ($duree[$i] == $r->tfduration) $selected = "selected";
                echo '<option value="'.$duree[$i].'" '.$selected.'>'.$duree[$i].'</option>';
            }
            ?>
            </select>
        </div>
    </div>

    <?php
        if ($action != "add"){
    ?>
    <div class="form-group row">
        <span class="col-sm-2 text-secondary">Création </span>
        <span class="col-sm-2 text-secondary"><?php echo $r->tfdatcrt;?> </span>
        <span class="col-sm-2 text-secondary"> <?php echo $r->tfusrcrt;?> </span>
    </div>
    <div class="row">
        <span class="col-sm-2 text-secondary">Modification </span>
        <span class="col-sm-2 text-secondary"><?php echo $r->tfdatupd;?></span>
        <span class="col-sm-2 text-secondary"> <?php echo $r->tfusrupd;?> </span>
    </div>
    <?php
        }
    ?>


    <div class="form-group row">
        <div class="col-sm-2">
            <?php
            if ($action <> "add"){
            echo form_hidden('tfid',$r->tfid);
            }

            if ($action <> 'vis'){
            echo form_submit('submit',$txtbouton, $classbouton);
            }

        
            echo form_close();
            ?>
        </div>
    </div>
</div>