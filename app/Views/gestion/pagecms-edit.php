
<?php
$index = 'index.php/';
$session = \Config\Services::session();
?>
<div class="container">
<h1 class="titrepage"><?= lang('Text.edit') ?> <?= lang('Text.page') ?></h1>
<?php

//
//$page="note";

//echo "page".$page;
helper('form');



if ($action == "add"){ 
    $url = base_url($index.'gestion/'.$page.'/add');
    echo form_open($url); 
    $txtbouton = lang('Text.add');
    $classbouton = "class='btn btn-primary'";
}
if ($action == "upd"){ 
    $url = base_url($index.'gestion/'.$page.'/upd');
    echo form_open($url); 
    $txtbouton = lang('Text.update');
    $classbouton = "class='btn btn-primary'";
}
if ($action == "del"){

     $url = base_url($index.'gestion/'.$page.'/del');
     echo form_open($url); 
     $txtbouton = "Suppression"; 
     $classbouton = "class='btn btn-danger'";}

?>
<?php
  if (!empty($erreurs)) {
    echo "<br/>";
    ?>
  <div class="alert alert-danger" role="alert">
 <p>Attention il y a au moins une erreur</p>
    <?php foreach ($erreurs as $erreur): ?>
        <li><?php echo $erreur; ?></li>
        <?php endforeach ?>
  </div>
  <?php } ?>

  <div class="form-group row">
    <span class="col-sm-2 col-form-label" ><?= lang('Text.code') ?></span>
        <div class="col-10">
        <?php


            $data = array(
                        'name'        => 'code',
                        'type'        => 'text',
                        'value'       =>  $r->code,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

  <div class="form-group row">
    <span class="col-sm-2 col-form-label" ><?= lang('Text.title') ?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'titre',
                        'type'        => 'text',
                        'value'       =>  $r->titre,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" ><?= lang('Text.shorttext') ?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'textecourt',
                        'type'        => 'text',

                        'value'       =>  $r->textecourt,
                        'style'       => 'width: 100%; height:3em;'
                        );
            echo form_textarea($data);
            ?>
        </div>
    </div>

  <div class="form-group row">
    <span class="col-sm-2 col-form-label" ><?= lang('Text.text') ?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'textelong',
                        'id'          => 'textareatinymce',
                        'type'        => 'text',
                        'value'       =>  $r->textelong,
                        'style'       => 'width: 100%'
                        );
            echo form_textarea($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" ><?= lang('Text.metatitle') ?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'metatitle',
                        'type'        => 'text',
                        'value'       =>  $r->metatitle,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" ><?= lang('Text.metadescription') ?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'metadescription',
                        'type'        => 'text',
                        'value'       =>  $r->metadescription,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" ><?= lang('Text.pagetype') ?></span>
        <div class="col-4">
            <select name="typepage" class="form-control">
            echo '<option value="">Content</option>';
                <?php 
                foreach($typepages as $row)
                { 
                    $selected ='';
                    if ($r->typepage == $row->paco) $selected = "selected";
                echo '<option value="'.$row->paco.'" '.$selected.'>'.$row->libelle.'</option>';
                }
                ?>
            </select>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" ><?= lang('Text.order') ?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'ordre',
                        'type'        => 'text',
                        'value'       =>  $r->ordre,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>


    <div class="form-group row">
    <span class="col-sm-2 col-form-label" ><?= lang('Text.visible') ?></span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'visible',
                        'type'        => 'text',
                        'value'       =>  $r->visible,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>
    <div class="form-group row">
    <span class="col-sm-2 col-form-label" ><?= lang('Text.datevalidityfrom') ?></span>
        <div class="col-10">
        <?php
                 $data = array(
                    'name'        => 'datdeb',
                    'type'        => 'date',
                    'value'       => $r->datdeb,
                    'style'       => 'width: 10em;'
                    );
        echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" ><?= lang('Text.datevalidityto') ?></span>
        <div class="col-10">
        <?php
                 $data = array(
                    'name'        => 'datfin',
                    'type'        => 'date',
                    'value'       => $r->datfin,
                    'style'       => 'width: 10em;'
                    );
        echo form_input($data);
            ?>
        </div>
    </div>

    <?php
    if ($action != "add"){
    ?>
    <div class="form-group row">
    <span class="col-sm-2 text-secondary"><?= lang('Text.creation') ?> </span>
    <span class="col-sm-2 text-secondary"><?php echo $r->datcrt;?> </span>
    <span class="col-sm-2 text-secondary"> <?php echo $r->usrcrt;?> </span>
</div>
<div class="row">
    <span class="col-sm-2 text-secondary"><?= lang('Text.update') ?></span>
    <span class="col-sm-2 text-secondary"><?php echo $r->datmod;?></span>
    <span class="col-sm-2 text-secondary"> <?php echo $r->usrmod;?> </span>
</div>
<?php
    }
    ?>

<div class="form-group row">
    <div class="col-sm-2">
    <?php
    if ($action <> "add"){
    echo form_hidden('id',$r->id);}

    if ($action <> 'vis'){
    echo form_submit('submit',$txtbouton, $classbouton);
    }
    echo form_close();
    ?>
    </div>
</div>
</div>