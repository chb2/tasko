<div class="container">
<h1 class="titrepage"><?php echo $view['title'] ;?></h1>
<p> <a href="<?php echo site_url('/gestion/'.$page.'/ajout');?>" class="btn btn-primary">Ajout</a></p>
<p> <a href="<?php echo site_url('/gestion/'.$page.'/liste/3');?>">A faire</a> -
<a href="<?php echo site_url('/gestion/'.$page.'/liste/5');?>">En cours</a> -
<a href="<?php echo site_url('/gestion/'.$page.'/liste/7');?>">Terminé</a> - 
<a href="<?php echo site_url('/gestion/'.$page.'/calculordre');?>">Calcul ordre</a></p>

    <table class="table table-responsive table-striped table-bordered">
        <tr>
            <th></th>
            <th>Projets</th>
            <th>durée réelle / estimée</th>
            <th>Status</th>
            <th>Réalisation</th>
            <th>Echéance</th>
            <th></th>
        </tr>
        <?php foreach ($t as $r): ?>
            <?php 
            $urledit = site_url('/gestion/'.$page.'/edit/'.$r->id);
            $urlvue = site_url('/gestion/'.$page.'/vue/'.$r->id);
            $urlsup = site_url('/gestion/'.$page.'/sup/'.$r->id);
            ?>
        <tr>
            <td>
                  <a href="<?php echo $urledit;?>">
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                  </a>  
            </td>
            <td>
            <?php
            if ($r->status =="5"){?>      
   
               <!--   <a href="<?php echo $urlvue;?>">
                  <?php echo $r->nom;?>
                  </a>-->

                  <span>      
                    <a class="klakebla listetasko" href="<?php echo $urlvue;?>">
                        <?php echo $r->nom;?>               
                    </a>
               </span>
            <?php }
            else
                  {
                   ?>  
 
               <?php echo $r->nom;?>
            <?php } ?>  

            </td>

            <td>
                  <?php echo $r->durationreal;
                  if ($r->durationreal > 0 ) echo '/';
                  echo $r->durationestim;?>
            </td>
            <td>
                  <?php
                  switch ($r->status) {
                        case '3':
                            echo "A venir";
                            break;
                        case '5':
                            echo "En cours";
                            break;
                        case '7':
                            echo "Terminé";
                            break;
                    }
                  ?>
            </td>

            <td>
                  <?php
                 echo $r->datreal; 
                  ?>
            </td>
          <?php  
          $style ="";
          if ($r->termindate < date("Y-m-d")) {$style='style="color:red;"';} 
            echo "<td ".$style.">";
            echo $r->termindate; 
            ?>
            </td>
            
<td>
<?php
if ($r->durationreal==0) {
echo "<a href=".$urlsup."><i class='fa fa-times text-danger' aria-hidden='true'></i></a>";

}?>

</td>



        </tr>
        <?php endforeach ?>
    </table>
</div>