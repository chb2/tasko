<div class="container">
    <h1 class="titrepage"><?php echo $view['title'] ;?></h1>
    <p> <a href="<?php echo site_url('/gestion/'.$dbtable.'/ajout');?>" class="btn btn-primary">Ajout</a></p>
    <p> <a href="<?php echo site_url('/gestion/'.$dbtable.'/liste/3');?>">A faire</a> -
    <a href="<?php echo site_url('/gestion/'.$dbtable.'/liste/5');?>">En cours</a> -
    <a href="<?php echo site_url('/gestion/'.$dbtable.'/liste/7');?>">Terminé</a> - 
    <a href="<?php echo site_url('/gestion/'.$dbtable.'/calculordre');?>">Calcul ordre</a></p>
    <?php
    $toth = 0;
    //$tdurationestim = 0;
    ?>
       <?php foreach ($t as $r): ?>
            <div class="row mx-1">
                <?php 
                $urledit = site_url('/gestion/'.$dbtable.'/edit/'.$r->tid);
                $urlvue = site_url('/gestion/'.$dbtable.'/vue/'.$r->tid);
                $urlsup = site_url('/gestion/'.$dbtable.'/sup/'.$r->tid);
                if (is_numeric($r->tdurationestim)) {$tdurationestim = $r->tdurationestim;}else{$tdurationestim = 0;}
                if (is_numeric($r->tdurationreal)) {$tdurationreal = $r->tdurationreal;}else{$tdurationreal = 0;}
                $h =  $tdurationestim - $tdurationreal;
                $toth =  $toth + $h;
                ?>
                 <div class="co-1" display:inline; >
                    <a href="<?php echo $urledit;?>">
                            <i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                    </a>  
                </div>
                <div class="col-8" >

                    <span>

         
                        <?php if ($r->tstatus <= "5") { ?>      
                            <a class="klakebla listetasko" href="<?php echo $urlvue;?>">
                                <?php echo $h.' '.$r->tnom;?>               
                            </a>
                        <?php 
                            }
                        else
                            {
                        ?>  
                        <?php echo $r->tnom;?>
                        <?php } ?>  
 
                    </span>
 
                 </div>
                 <div class="col" white-space: nowrap; >
                   <?php  
                            $style ="";
                            if ($r->ttermindate < date("Y-m-d")) {$style='style="color:red;"';} 
                            echo "<span ".$style." white-space: nowrap;> ";
                            echo ''.substr($r->ttermindate,2,8); 
                            echo "</span>";
                    ?>
                 </div>
            </div>
        <?php endforeach ?>
        <div class="row mx-1">
            <div class="col-1" display:inline; >

            </div>
            <div class="col-8" >
            <?php
            echo "total : ".$toth;
            ?>
        </div>
        </div>


</div>