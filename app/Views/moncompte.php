<div class="container">
    <h1 class="titrepage"><?php echo $view['title'] ;?></h1>
    <p>Gestion de mon compte</p>
    <?php
    if ($action=='validationemail')
    {
      echo '  <p>Validation email</p>';
    }

    if ($action=='erreur')
    {
      echo "  <p>Quelque chose n'est pas bon </p>";
    }

    if ($action=='valide')
    {
      echo "  <p>Compte email est maintenant bien validé. </p>";
      echo "  <p>En cas de perte de mot de passe cet email est maintenant un email de confiance. </p>";
  
    }


    if ($action=='demchgpwd')
    {
      echo "  <p>Changement de mot de passe ".$r->code."</p>";

      if (!empty($msgerr)) 
      {
        echo "<br/>";
        ?>
        <div class="alert alert-danger" role="alert">
    
 
          <li><?php echo $msgerr; ?></li>
 
        </div>
      <?php 
      }  


      helper('form');
      echo form_open($page.'/changementpwdexe'); 
      ?>
        <div class="form-group row">
          <span class="col-sm-2 col-form-label" >Mot de passe</span>
          <div class="col-10">
            <?php

                $data = array(
                            'name'        => 'pasvor',
                            'type'        => 'text',
                            'value'       => ' ',
                            'style'       => 'width: 100%'
                            );
                echo form_password($data);

            ?>
          </div>
        </div>

        <div class="form-group row">
          <span class="col-sm-2 col-form-label" >Confirmation Mot de passe</span>
          <div class="col-10">
            <?php

                $data = array(
                            'name'        => 'pasvor2',
                            'type'        => 'text',
                            'value'       => ' ',
                            'style'       => 'width: 100%'
                            );
                echo form_password($data);

            ?>
          </div>
        </div>

        <?php
     echo form_hidden('id',$r->id);
     $txtbouton = "Valider";
     $classbouton = "class='btn btn-primary'";
      echo form_submit('submit',$txtbouton, $classbouton);

      echo form_close();

    }

    if ($action=='motdepassemisajour')
    {
      echo "  <p>Mot de passe modifié. </p>";
      echo "  <p>Vous pouvez maintenant vous connecter </p>";
  
    }

    if ($action=='motcompteinfo')
    {
      echo $msgerr; 
  
    }

    ?>
</div>