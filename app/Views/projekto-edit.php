<div class="container">
    <h1 class="titrepage"><?php echo $view['title'] ;?></h1>
 
    <?php
    helper('form');

    if ($action == "add"){ echo form_open('gestion/'.$page.'/add'); $txtbouton = "Ajout";}
    if ($action == "upd"){ echo form_open('gestion/'.$page.'/upd'); $txtbouton = "Mise à jour";}
    if ($action == "del"){ echo form_open('gestion/'.$page.'/del'); $txtbouton = "Suppression";}

    if ($action == "add"){ 
        echo form_open($page.'/add'); 
        $txtbouton = "Ajout";
        $classbouton = "class='btn btn-primary'";
    }
    if ($action == "upd"){ 
        echo form_open($page.'/upd'); 
        $txtbouton = "Mise à jour";
        $classbouton = "class='btn btn-primary'";
    }
    if ($action == "del"){
        echo form_open($page.'/del'); 
        $txtbouton = "Suppression"; 
        $classbouton = "class='btn btn-danger'";}
    ?>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Tâche</span>
        <div class="col-10">
            <?php
            $data = array(
                        'name'        => 'nom',
                        'type'        => 'text',
                        'value'       => $r->nom,
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Description</span>
        <div class="col-10">
        <?php
    //  if ($action == "add"){ $tfdate =  date('Y-m-d');} else{$tfdate = $r['tfdate'];}
        $data = array(
                    'name'        => 'desc',
                    'type'        => 'text',
                    'value'       => $r->desc,
                    'style'       => 'width: 100%'
                    );
        echo form_textarea($data);
        ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Catégorie</span>
        <div class="col-4">
            <select name="categ" class="form-control">
                <?php 
                foreach($categs as $row)
                { 
                    $selected ='';
                    if ($r->categ == $row->paco) $selected = "selected";
                echo '<option value="'.$row->paco.'" '.$selected.'>'.$row->libelle.'</option>';
                }
                ?>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Durée réelle en heure</span>
        <div class="col-10">
            <?php echo $r->durationreal;?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Durée estimée en heures</span>
        <div class="col-10">
        <?php
        $data = array(
            'name'        => 'durationestim',
            'type'        => 'text',
            'id'          => 'durationestim',
            'value'       => $r->durationestim,
            'style'       => 'width: 100%'
       );
        echo form_input($data);
        ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Date échéance</span>
        <div class="col-10">
            <?php
            $data = array(
                'name'        => 'termindate',
                'type'        => 'date',
                'id'          => 'termindate',
                'value'       => $r->termindate,
                'style'       => 'form-control'
        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
        <span class="col-sm-2 col-form-label" >Etat</span>
        <div class="col-10">
            <select name="status">
            <?php
            if ($action == "add") 
            {
                $stat = '5';
            }
            else
            {
                $stat = $r->status;
            }
            // 3 à venir  5 en cours et 7 cloturé
            $seladmin="";
            $sel3 = "";
            $sel5 = "";
            $sel7 = "";
            if ($stat == "3"){$sel3 = "selected";}
            if ($stat == "5"){$sel5 = "selected";}
            if ($stat == "7"){$sel7 = "selected";}
            ?>
            <option value="3" <?php echo $sel3; ?>>A venir</a>
            <option value="5" <?php echo $sel5; ?>>En cours</a>
            <option value="7" <?php echo $sel7; ?>>Cloturé</a>
            </select>
        </div>
    </div>

    <?php
    if ($action != "add"){
    ?>
        <div class="form-group row">
            <span class="col-sm-2 text-secondary">Propriétaire</span>
            <span class="col-sm-2 text-secondary"> <?php if (isset($r->uzanto)){echo $r->uzanto;}?></span>
            <span class="col-sm-2 text-secondary"> <?php echo $r->datreal;?> </span>
        </div>

        <div class="form-group row">
            <span class="col-sm-2 text-secondary">Création </span>
            <span class="col-sm-2 text-secondary"><?php echo $r->datcrt;?> </span>
            <span class="col-sm-2 text-secondary"> <?php echo $r->usrcrt;?> </span>
        </div>
        <div class="row">
            <span class="col-sm-2 text-secondary">Modification </span>
            <span class="col-sm-2 text-secondary"><?php echo $r->datupd;?></span>
            <span class="col-sm-2 text-secondary"> <?php echo $r->usrupd;?> </span>
        </div>
        <?php
        if (isset($r->uzanto)){
        echo form_hidden('uzanto',$r->uzanto);
        }
    }
    ?>

    <div class="form-group row">
    <div class="col-10"></div>
    <div class="col-2">
        <?php

        if ($action <> "add"){
        echo form_hidden('id',$r->id);}

        if ($action <> 'vis'){
        echo form_submit('submit',$txtbouton, $classbouton);
        }

        echo form_close();
        ?>
        </div>
    </div>
</div>