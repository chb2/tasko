<div class="container">
    <h1 class="titrepage"><?php echo $t->tnom;//echo $view['title'] ;?></h1>
    <div class="row grey-border bg-grey">
        <div class="col-sm-9" >
        <p class="titredesctache"><?php echo $t->tdesc; ?> </p>
        </div>
        <div class="col-sm-3" >
        <p><?php echo 'Date échéance : '.$t->ttermindate;?></p>
        </div>
    </div>

    <div class="form-group row grey-border bg-grey form-grid">
        <div class="col-sm-12" >
        <?php
        helper('form');
        echo form_open('gestion/tasko/addtf');


        $data = array(
            'name'        => 'tfduration',
            'style'       => 'width:10%'
        );
        echo form_input($data);
        echo 'h';
        $data = array(
        'name'        => 'tfdesc',
        'style'       => 'width:70%;margin-left:10px;'
        );
        echo form_input($data);

        $wudate = date('Y-m-d');
        echo form_hidden('tfdate',$wudate);
        echo form_hidden('tftid',$t->tid);
        $classbouton = "class='btn btn-primary'";
        echo form_submit('submit','Ajout réalisation', $classbouton);
        echo form_close();
        $totalduration = 0;
        ?>
    
        </div>
 
    </div>
    <br/>
    <div class="row">
    <div class="col-sm-12" >
      <table class="table table-responsive table-striped table-bordered">
      <?php  foreach ($tf as $r): ?>
        <tr>
         <td>
            <?php echo $r->tfdate;?>
          </td>
          <td>
            <?php echo $r->tfdesc;?>
          </td>
          <td>
            <?php echo $r->tfduration;
            $totalduration = $totalduration + $r->tfduration;
            ?>
          </td>
        </tr>
      <?php endforeach ?>
        <tr>
          <td></td>
          <td>Total</td>
          <td><?php echo $totalduration;?></td>
        </tr>
      </table>

    </div>
  </div>


</div>