<?php

namespace App\Controllers;
use App\Models\PageModel;
use \App\Controllers\Page;
use App\Models\PagecmsModel;


class Home extends BaseController
{
    public function index()
    {
       
       // return view('welcome_message');
       $data['view']['title'] = "Accueil";
       $data['page'] = "home";

       $pagecms = new PagecmsModel();
       $data['r'] =  $pagecms->get1percode('accueil');
       
       $page = new PageModel();
       $page->affiche($data);

    }
}
