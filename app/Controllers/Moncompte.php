<?php

namespace App\Controllers;
use App\Models\PageModel;
use App\Models\UzantoModel;
use \App\Controllers\Page;


class Moncompte extends BaseController
{
    public function index()
    {
       
       // return view('welcome_message');
       $data['view']['title'] = "Mon compte";
       $data['page'] = "moncompte";
        $page = new PageModel();
        $page->affiche($data);
    }

    public function validationemail()
    {
       
       // return view('welcome_message');
       $data['view']['title'] = "Mon compte";
       $data['page'] = "moncompte";
       $data['action'] = "validationemail";
        // il faudra encore contrôler que j'ai quelque chose


       $url = current_url();
       $a = explode("/",$url);
       $nb = count($a);

       for ($i = 0; $i < $nb; $i++) {
         if ($a[$i]=="validationemail") $pos = $i;
       }
       $dif = $nb-$pos;
  
       // on compte le nombre d'élément il y a dans l'url pour une protection
     if ($dif == 3){
         $i = $pos +1;
         $id = $a[$i];
         $i = $i+1;
         $cle = $a[$i];
       //  echo '<br/>id'.$id;
       //  echo '<br/>cle'.$cle;
         // il faut chercher la clé et la comparer. Si elle est bonne on valide update sinon erreur

         $uzanto = new UzantoModel();
         $u = $uzanto->get1uzanto($id);
 
         if ($u->slosilo == $cle and $u->slosilo >'')
         {


            $d['datvalemail'] = date('Y-m-d');
            $d['slosilo'] = '';
            $d['datmod'] = date('Y-m-d H:m:s');
            $db = db_connect();
            $builder = $db->table('uzanto');
            $builder->where('id', $id);
            $builder->update($d);

            $data['action'] = "valide";
         }
         else
         {
            $data['action'] = "erreur";
         }
       


         $data['view']['title'] = "Mon compte";
         $data['page'] = "moncompte";
         $data['id'] = $id;
         $data['cle'] = $cle;
         $page = new PageModel();
         $page->affiche($data);

     }
     else
     {
        // validation non ok
        $data['view']['title'] = "Mon compte";
        $data['page'] = "moncompte";
        $data['action'] = "erreur";
        $page = new PageModel();
      $page->affiche($data);
     }

    }

    public function changementpwd()
    {
      $url = current_url();
      $a = explode("/",$url);
      $nb = count($a);

      for ($i = 0; $i < $nb; $i++) {
        if ($a[$i]=="changementpwd") $pos = $i;
      }
      $dif = $nb-$pos;
      // on compte le nombre d'élément il y a dans l'url pour une protection
      if ($dif <> 3){
      exit;
      }
      if ($dif == 3){
        $i = $pos +1;
        $id = $a[$i];
        $i = $i+1;
        $cle = $a[$i];
       // echo 'id'.$id;
       // echo 'cle'.$cle;
      }
      // lecture uzanto
      $uzanto = new UzantoModel();
      $u = $uzanto->get1uzanto($id);
      
      $erreur = 1;
      if ( (isset($u->slosilo)) and ($u->slosilo == $cle) and ($u->datlimslo > date("Y-m-d H:i:s")) )
      {
          $erreur = 0; 
          // validation non ok sur la date limite
          $data['view']['title'] = "Changement mot de passe";
          $data['page'] = "moncompte";
          $data['action'] = "demchgpwd";
          $data['r'] = $u;
          $page = new PageModel();
          $page->affiche($data);
      }
      else
      {
        //echo 'marche pas';
        // validation non ok
        $data['view']['title'] = "Mon compte";
        $data['page'] = "moncompte";
        $data['action'] = "erreur";
        $page = new PageModel();
        $page->affiche($data);
      }


    }
    public function changementpwdexe()
    {
      $id = $this->request->getVar('id');
      $uzanto = new UzantoModel();
      $u = $uzanto->get1uzanto($id);
      $pasvor = $this->request->getVar('pasvor');
      $pasvor2 = $this->request->getVar('pasvor2');
      //  contrôle si le mot de passe est identique
      $erreur = 0;
      if ($pasvor  <> $pasvor2 )
      {
        $data['msgerr'] = "Mot de passe différent";
        $erreur = 1;

      }
      if ( ($erreur == 0)  and (strlen($pasvor) < 4))
      {
        $data['msgerr'] = "Longeur du mot de passe minimum 4";
        $erreur = 1;

      }
      if ($erreur ==1)
      {
        // validation non ok sur la date limite
        $data['view']['title'] = "Changement mot de passe";
        $data['page'] = "moncompte";
        $data['action'] = "demchgpwd";
        $data['r'] = $u;
        $page = new PageModel();
        $page->affiche($data);
      }
      else
      {
            // mise $ jour
      $d['datvalemail'] = date('Y-m-d');
      $d['slosilo'] = '';
      $d['pasvor'] =  md5($this->request->getVar('pasvor'));
      $db = db_connect();
      $builder = $db->table('uzanto');
      $builder->where('id', $id);
      $builder->update($d);


      // afficher un écran
      $data['view']['title'] = "Mon compte";
      $data['page'] = "moncompte";
      $data['action'] = "motdepassemisajour";
      
      $page = new PageModel();
      $page->affiche($data);

      }


    }
}
