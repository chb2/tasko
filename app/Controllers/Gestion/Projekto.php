<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\PageModel;
use App\Models\ToolModel;
use App\Models\TaskoModel;
use App\Models\ParamModel;
use App\Models\ProjektoModel;
 

class Projekto extends BaseController {
    public static  $page = 'projekto';
    public static  $table = 'projekto';

    public function __construct()
    {
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
    }
  

    public function index()
	{
        $this->liste('param');
   
	}

    public function liste($type = '5')
	{
        
        $data['view']['title'] = "Projets";
        $data['table'] = self::$table;
        $data['page'] = self::$page;


        $p['status'] = $type;
        $session = \Config\Services::session();
        $p['userapp'] = $session->userapp;
    //   echo  $p['userapp'] ;
    //    exit;
        if ($type == '3') $p['status'] = '3';
        if ($type == '7') $p['status'] = '7';
        if ($type == '0') $p['status'] = '0';
        $model = new ProjektoModel();
        $data['t'] = $model->getProjektoj($p);

        $t = $data['t'] ;


        $page = new PageModel();
        $page->gestion($data,'projekto-liste');

      //  $page->showme('user-liste',$data);
    }

    function ajout(){

        $session = \Config\Services::session();
        //  $data['dbtable'] = "tasko";
        $data['page'] = self::$page;
        $data['view']['title'] = "Ajout d'un projet";
        $data['action'] = 'add';

        $r = new class{};
        $r->nom = "";
        $r->desc = '';
        $r->durationreal = 0;
        $r->durationestim = 0;
        $r->termindate = "";
        $r->categ = "";
        $data['r'] = $r;

        $param = new ParamModel();
        $data['categs'] = $param->getparams('categ');

        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }

    function add(){
        $d = $this->addupd('a');
       // print_r($d);
        $db = db_connect();
        $db->table('projekto')->insert($d);
        $this->liste();
	}

    function edit($id){
        $data['page'] = self::$page;
        $data['view']['title'] = "Modification d'un projet";
        $data['action'] = 'upd';

        $projekto = new ProjektoModel();
        $data['r'] = $projekto->get1projekto($id);
        


       // $model = new TaskerofaritaModel();
       // $data['nbtf'] = $model->getNbTaskerofaritaj($id);

        $param = new ParamModel();
        $data['categs'] = $param->getparams('categ');


     //   $page = new Page();
     //   $page->showme('tasko-edit',$data);

        $page = new PageModel();
        $page->gestion($data,'projekto-edit');
   
    }

    public function upd() {
        $id = $this->request->getVar('id');
        echo $id;
        $d = $this->addupd('u');
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->update($d);
         $this->liste();
    }


    function addupd($t){
        $wudate = date('Y-m-d');
        $session = \Config\Services::session();

        $d['nom'] = $this->request->getVar('nom');
        $d['desc'] = $this->request->getVar('desc');
        $d['durationestim'] = $this->request->getVar('durationestim');
        $d['termindate'] = $this->request->getVar('termindate');
        $d['status'] = $this->request->getVar('status');
        $d['categ'] = $this->request->getVar('categ');
        $d['datupd'] = $wudate;
        $d['usrupd'] = $session->userapp;
        if ($t=="a"){
            $d['datcrt'] = $wudate;
            $d['usrcrt'] =  $session->userapp;
            $d['uzanto'] =  $session->userapp;
            }
        return $d;
        }

    function sup($id){
        $data['dbtable'] = "tasko";
        $data['page'] = self::$page;
       // $data['view']['title'] = "Modification d'une tache";
        $data['view']['title'] = "Suppression";
        $data['action'] = 'del';

        $projekto = new ProjektoModel();
        $data['r'] = $projekto->get1projekto($id);

        $param = new ParamModel();
        $data['categs'] = $param->getparams('categ');

        $tasko = new TaskoModel();
        $nb = $tasko->getNbTaskosProjekto($id);
        if ($nb > 0)    {
            $data['action'] = 'upd';
            $data['view']['title'] = "Modification";
            $data['erreurs'][] = "Suppression impossible : ce projet contient des taches (".$nb.")";
        }
        else
        {
            $data['action'] = 'del';
            $data['view']['title'] = "Suppression";
        }


        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }
    /* ===== suppression reel ===== */
    function del(){
        $id = $this->request->getVar('id');
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->delete();
        $this->liste();
    }

    public function vue($id)
    {
        $data['dbtable'] = "projekto";
        $data['view']['title'] = "Projet";


        $projekto = new ProjektoModel();
        $p = $projekto->get1projekto($id);
        $data['p'] = $p;

        $param = new ParamModel();
        $paco = $p->categ;
        $data['categ'] = $param->getlibelle('categ',$paco);
        // état
        $tasko = new TaskoModel();
        $data['statusName'] = $tasko->getStatusName($p->status);
        // chercher les taches d'un projet
        $t = $tasko->getTaskosProjekto($id);
        $data['t'] = $t;



        $page = new PageModel();
        $page->gestion($data,'projektov');
    }

    function calculordre(){

        //$session = \Config\Services::session();
        //$typevue = $session->typevue;
        $projekto = new ProjektoModel();
        $projekto->calculordre();
        $this->liste(); 
    
    }

    public function crttab()
	{
        $projekto = new ProjektoModel();
        $projekto->CreationTable();
	}

    function updtab1(){
        $tool = new ToolModel();
        $tool->addfield('projekto','ordre','INTEGER');            
    }

}
