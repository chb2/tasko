<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\PageModel;
use App\Models\LoginModel;
use App\Models\UzantoModel;

class Login extends BaseController
{
    
    
    public function index()
	{
       
       $data['msgerr'] = "";
       $page = new PageModel();
       $page->gestion($data,'login');
	}

    public function logincheck()
	{

       $u = $this->request->getVar('u');
       $p = $this->request->getVar('p');
       $login = new LoginModel();
       $data = $login->logincheck($u,$p);
 
    }

    public function demchgpas1()
	{
        echo 'demchgpas';
        $data['msgerr'] = "Pour demander à changer votre mot de passe, vous devez avoir un email validé. Vous recevrez les instructions sur cet email";
        $page = new PageModel();
        $page->gestion($data,'login-dempas');
    }

    public function demchgpas2()
	{
       // echo 'Etape 2';
        $p['email1'] = $this->request->getVar('email1');
       // echo $this->request->getVar('email1');
        $uzanto = new UzantoModel();
        $uzanto->demchgpas2($p);
        // envoi email 
      /* $data['msgerr'] = "Pour demander à changer votre mot de passe, vous devez avoir un email validé. Vous recevrez les instructions sur cet email";
        $page = new PageModel();
        $page->gestion($data,'login-dempas');*/

        $data['view']['title'] = "Changement mot de passe";
        $data['msgerr'] = "Un email vient de vous être envoyé";
        $data['page'] = "moncompte";
        $data['action'] = "motcompteinfo";
        //$data['r'] = $u;
        $page = new PageModel();
        $page->affiche($data);
    }
    
    function logout(){
        //echo 'logout';
        $session = \Config\Services::session();
        $session->remove('userapp');
        //echo site_url('');
       // echo "<a href=".site_url('').">Au revoir</a>";

        $data['view']['title'] = "Merci";
        $data['page'] = "home";
        $page = new PageModel();
        $page->affiche($data);

        }





}