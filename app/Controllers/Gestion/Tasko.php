<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\PageModel;
use App\Models\TaskoModel;
use App\Models\ParamModel;
use App\Models\ProjektoModel;
use App\Models\TaskerofaritaModel;
use App\Controllers\Gestion\Projekto;
use App\Models\SprintModel;

class Taskor {
    public int    $projektoid;
    public string $tnom; 
    public string $tdesc; 
    public string $tcateg; 
    public int    $tpriorit; 
    public int    $ordre; 
    public int    $tdurationreal; 
    public int    $tdurationestim; 
    public string $ttermindate; 
    public string $tstatus; 
    public int     $sprintid = 0; 
  }
class Tasko extends BaseController {
    public static  $page = 'tasko';
    public static  $table = 'tasko';

    public function __construct()
    {
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
    }

    public function index()
    {
        $this->liste('param');

    }

    public function liste($type = '5')
	{
        $session = \Config\Services::session();
        $data['view']['title'] = "Tâches en cours";
        $data['dbtable'] = "tasko";
        $data['table'] = self::$table;
        $data['page'] = self::$page;
        
        $p['tstatus'] = $type;
        //$session->tstatus = '5';
        if ($type == '3') {
            $p['tstatus'] = '3';
            $session->typevue =  '3' ;
            $data['view']['title'] = " Tâches à faire";
        }
        if ($type == '5')
        {
           $session->typevue =  '5' ;
        }
        if ($type == '7')
        {
            $p['tstatus'] = '7';
            $session->typevue =  '7' ;
            $p['order'] = 'tdatreal';
            $data['view']['title'] = "Tâches terminées";
        }
        if ($type == '0') $p['tstatus'] = '0';

        $model = new TaskoModel();
        $data['t'] = $model->getTaskoj($p);
        $t = $data['t'] ;

        $page = new PageModel();
        $page->gestion($data,'tasko-liste');
     //   echo 'Type fin : '.$session->tstatus;

      //  $page->showme('user-liste',$data);
    }


    function ajout(){
        // quand on vient de projekto on passe projektoid et reiro = 'projekto'
        $session = \Config\Services::session();
        $projektoid  = $this->request->getVar('projektoid');
        if (!isset($projektoid)){  $projektoid =0;}
        $data['projektoid']  = $this->request->getVar('projektoid');
        $data['reiro']  = $this->request->getVar('reiro');
        $data['dbtable'] = "tasko";
        $data['view']['title'] = "Ajout d'une tache";
        $data['action'] = 'add';
        $date14 = date("Y-m-d", strtotime(date('Y-m-d').'+ 30 days'));
        $r = new Taskor();
        $r->projektoid =  $projektoid ;
        $r->tnom = '';
        $r->tdesc = '';
        $r->tdurationreal = 0;
        $r->tdurationestim = 0;
        $r->tpriorit = 50;
        $r->ttermindate = date("Y-m-d", strtotime(date('Y-m-d').'+ 21 days'));
        $r->tcateg = "";
        $r->tstatus = "3";
        $r->ordre = 50;
        $data['r'] = $r;

        $projekto = new ProjektoModel();
        $data['ldprojekto'] = $projekto->getProjektoDropDown();

        $sprint = new SprintModel();
        $data['ldsprint'] = $sprint->getSprintDropDown();

        $param = new ParamModel();
        $data['categs'] = $param->getparams('categ');

        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }

    function add(){
        $reiro  = $this->request->getVar('reiro');
        $projektoid  = $this->request->getVar('projektoid');
        $d = $this->addupd('a');
        $db = db_connect();
        $db->table('tasko')->insert($d);
        if ($reiro == 'projekto' )
        {
           // echo 'revenir au projet'.$projektoid;
            $projektoController = new Projekto();
            $projektoController->vue($projektoid);
        }
        else
        {
            $this->liste($type = '3');
        }

	}

    function edit($id){
        $data['projektoid']  = $this->request->getVar('projektoid');
        $data['reiro'] = $this->request->getVar('reiro');
        $data['dbtable'] = "tasko";
        $data['page'] = self::$page;
        $data['view']['title'] = "Modification d'une tache";
        $data['action'] = 'upd';
        $strsql = "select * from tasko where tid = $id";
        $db = db_connect();
        $query = $db->query($strsql);
        $data['r'] = $query->getRowArray();


        $tasko = new TaskoModel();
        $data['r'] = $tasko->get1tasko($id);


        $projekto = new ProjektoModel();
        $data['ldprojekto'] = $projekto->getProjektoDropDown();

        $sprint = new SprintModel();
        $data['ldsprint'] = $sprint->getSprintDropDown();


        $model = new TaskerofaritaModel();
        $data['nbtf'] = $model->getNbTaskerofaritaj($id);

        $param = new ParamModel();
        $data['categs'] = $param->getparams('categ');


     //   $page = new Page();
     //   $page->showme('tasko-edit',$data);

        $page = new PageModel();
        $page->gestion($data,'tasko-edit');
   
    }

    public function upd() {
        $reiro = $this->request->getVar('reiro');
        $tasko = new TaskoModel();
        helper(['form', 'url']);
        $validation =  \Config\Services::validation();
        $validation->setRule('tnom', 'tnom', 'required');

        if (! $this->validate([
            'tnom' => "required"]))
        {
          //echo 'pas bon ';
           $errors = $validation->getErrors();
          // var_dump($errors);
           //$this->edit($id);
           $data['dbtable'] = "tasko";
           $data['view']['title'] = "Modification d'une tache";
           $data['errors'] = 'Saisir une tâche';
           $data['action'] = 'upd';
           $id = $this->request->getVar('tid');
           $strsql = "select * from tasko where tid = $id";
         //  echo $strsql;
           $db = db_connect();
           $query = $db->query($strsql);
           $data['r'] = $query->getRowArray();



   
            $page = new Page();
           $page->showme('tasko-edit',$data);
           
        }
        else
        {
        //    echo 'bon';
 

        $date = date('Y-m-d');
 
        $id = $this->request->getVar('tid');
        $session = \Config\Services::session();
        $d = [
            'tnom'  => $this->request->getVar('tnom'),
            'tdesc'  => $this->request->getVar('tdesc'),
            'tdurationestim'  => $this->request->getVar('tdurationestim'),
            'ttermindate'  => $this->request->getVar('ttermindate'),
           
            'tstatus'  => $this->request->getVar('tstatus'),
            'tdatupd' =>  $date ,
            'tusrupd' => $session->userapp
        ];
        $d = $this->addupd('u');
        $db = db_connect();
        $builder = $db->table('tasko');
        $builder->where('tid', $id);
        $builder->update($d);

        $t = $tasko->get1tasko($id);
        //echo "On va maintenant mettre à jour le projet".$t->projektoid;
        $projektoid = $t->projektoid;
        $projekto = new ProjektoModel();
        $projekto->updProjektoFromTasko( $projektoid);
 
        $type = $session->tstatus;
      //  $this->liste(); 
      //  echo 'reiro'.$reiro;
      
            if ($reiro == 'projekto' )
            {
            // echo 'revenir au projet'.$projektoid;
                $projektoController = new Projekto();
                $projektoController->vue($projektoid);
            }
            else
            {
                $this->liste($type = '5');
            }





        }
    }



    function addupd($t){
        $wudate = date('Y-m-d');
        $session = \Config\Services::session();
        $d['projektoid'] = $this->request->getVar('projektoid');
        $d['sprintid'] = $this->request->getVar('sprintid');

        $d['tnom'] = $this->request->getVar('tnom');
        $d['tdesc'] = $this->request->getVar('tdesc');
        $d['tpriorit'] = $this->request->getVar('tpriorit');
        $d['tdurationestim'] = $this->request->getVar('tdurationestim');
        $d['ttermindate'] = $this->request->getVar('ttermindate');
        $d['tstatus'] = $this->request->getVar('tstatus');
        $d['tcateg'] = $this->request->getVar('tcateg');
        $d['tdatupd'] = $wudate;
        $d['tusrupd'] = $session->userapp;
        if ($t=="a"){
            $d['tdatcrt'] = $wudate;
            $d['tusrcrt'] =  $session->userapp;
            $d['tuzanto'] =  $session->userapp;
            }
        return $d;
        }

        public function vue($id)
        {
            $data['dbtable'] = "tasko";
            $data['view']['title'] = "Réalisation d'une tache";

            $tasko = new TaskoModel();
            $t = $tasko->get1tasko($id);
            $data['t'] = $t;
            $param = new ParamModel();
            $paco = $data['t']->tcateg;

           // $tasko = new TaskoModel();


            $data['r'] = $tasko->get1tasko($id);

            // recherche du nom du projet
            $projekto = new ProjektoModel();
            $pr = $projekto->get1Projekto($t->projektoid);
            $data['projektoNom'] = $pr->nom;
            $data['categ'] = $param->getlibelle('categ',$paco);
            $data['statusName'] = $tasko->getStatusName($t->tstatus);
            // echo $categ;
        
            $session = \Config\Services::session();
            $p['userapp'] = $session->userapp;
            $p['useradmin'] = $session->useradmin;
            $p['tid'] = $id;
            $model = new TaskerofaritaModel();
            $data['tf'] = $model->getTaskerofaritaj($p);
        

            $page = new PageModel();
            $page->gestion($data,'tasko-tf');
        }
        public function addtf()
        {
            $tasko = new TaskoModel();
            $session = \Config\Services::session();
            $tfduration =1;
            if ($this->request->getVar('tfduration') > 0){$tfduration = $this->request->getVar('tfduration');}
            $id = $this->request->getPostGet('tftid');
            $wudate = date('Y-m-d');
            $d = [
                'tftid'  => $this->request->getPostGet('tftid'),
                'tfuzanto' => $session->userapp,
                'tfdate'  => $this->request->getVar('tfdate'),    
                'tfdesc'  => $this->request->getVar('tfdesc'),    
                'tfduration'  => $tfduration,      
                'tfdatcrt' =>  $wudate ,
                'tfusrcrt' => $session->userapp,
                'tfdatupd' =>  $wudate ,
                'tfusrupd' => $session->userapp
            ];
           // var_dump($d);
            
            $db = db_connect();
            $db->table('taskerofarita')->insert($d);
            // mise à jour de la table tasko
        
            // calul de la durée total réalisé et date de dernière tache réalisée
 
            $tasko->updDurationreal($id);
            $tasko->updDateReal($id,$wudate);  

            // chercher le projet
          // $tasko = new TaskoModel();
            $t = $tasko->get1tasko($id);
           // echo "On va maintenant mettre à jour le projet".$t->projektoid;
           $projektoid = $t->projektoid;
            $projekto = new ProjektoModel();
            $projekto->updProjektoFromTasko( $projektoid);
        
            $this->vue($id);
        
        }

        public function cloturer($id)
        {
           // echo 'cloturer'.$id;
            $model = new TaskoModel();
            $model->cloturer($id);
            $this->liste($type = '7');
        }

        public function commencer($id)
        {
         //   echo 'commencer'.$id;
            $model = new TaskoModel();
            $model->commencer($id);
            $this->liste($type = '5');
        }
    function calculordre(){

        $session = \Config\Services::session();
        $typevue = $session->typevue;
        $tasko = new TaskoModel();
        $tasko->calculordre();
        $this->liste($typevue); 
    
    }
        function sup($id){

            $data['dbtable'] = "tasko";
            $data['page'] = self::$page;
           // $data['view']['title'] = "Modification d'une tache";
            $data['view']['title'] = "Suppression";
            //$data['action'] = 'upd';
            $data['action'] = 'del';

           /* $strsql = "select * from tasko where tid = $id";
            $db = db_connect();
            $query = $db->query($strsql);*/

            $tasko = new TaskoModel();
            $data['r'] = $tasko->get1tasko($id);
            
           // $data['r'] = $query->getRowArray();
    
            $projekto = new ProjektoModel();
            $data['ldprojekto'] = $projekto->getProjektoDropDown();
    
            /*$tasko = new TaskoModel();
            $data['r'] = $tasko->get1tasko($id);*/
    
    
            $model = new TaskerofaritaModel();
            $data['nbtf'] = $model->getNbTaskerofaritaj($id);
            
            $param = new ParamModel();
            $data['categs'] = $param->getparams('categ');
        
        
            $page = new PageModel();
            $page->gestion($data,self::$page.'-edit');
        
        
        }

        /* ===== suppression reel ===== */
        function del(){

                    $id = $this->request->getVar('id');
                    $db = db_connect();
                    $builder = $db->table(self::$table);
                    $builder->where('tid', $id);
                    $builder->delete();
                    $this->liste();
        
        }
        /* ===== suppression reel ===== */
    function del2($id){
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('tid', $id);
        $builder->delete();
        $this->liste();                
    }

    function updtab1(){
        $tasko = new TaskoModel();

        $tasko->addfield('projektoid','INTEGER');
        $tasko->addfield('ordre','INTEGER');
              
    }

    function updtab2(){
        $tasko = new TaskoModel();

        $tasko->addfield('sprintid','INTEGER');

              
    }

}