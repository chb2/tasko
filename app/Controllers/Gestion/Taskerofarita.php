<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\PageModel;
use App\Models\TaskerofaritaModel;
use App\Models\TaskoModel;

class Taskerofarita extends BaseController {
    public static  $page = 'taskerofarita';
    public static  $table = 'taskerofarita';

    public function __construct()
    {
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
    }

    public function index()
    {
      //  echo 'taskerofarota';

        $this->liste('param');

    }

  public function liste()
	{
    $session = \Config\Services::session();

    $data['view']['title'] = "Réalisations";
    $data['dbtable'] = self::$table;
    $p['tid'] = 0;

    if ($this->request->getVar('aadatseldeb') == ""){
      $aadatseldeb  =  date("Y-m-d", strtotime(" - 2 month "));
    }
    else
    {
      $aadatseldeb  =  $this->request->getVar('aadatseldeb');
    }
    if ($this->request->getVar('aadatselfin') == ""){
      $aadatselfin  =  date("Y-m-d");
    }
    else
    {
      $aadatselfin  =  $this->request->getVar('aadatselfin');
    }
    // pour le model
    $p['aadatseldeb']  = $aadatseldeb ;
    $p['aadatselfin']  =  $aadatselfin ;
    // pour la page
    $data['aadatseldeb']  = $aadatseldeb ;
    $data['aadatselfin']  = $aadatselfin ;

    $model = new TaskerofaritaModel();
    $p['useradmin'] = $session->useradmin;
    $p['userapp'] = $session->userapp;
    $data['useradmin'] = $session->useradmin;
    $data['tf'] = $model->getTaskerofaritaj($p);
     /*   $data['table'] = self::$table;
        $data['page'] = self::$page;
        // a remplacer par une méthode getuzantoj('id')
        $strsql = "select * from uzanto order by id";
        $db = db_connect();
        $query = $db->query($strsql);
       $data['t'] = $query->getResult();

       $uzanto = new UzantoModel();
       $data['uzantoj'] = $uzanto->getuzantoj();*/

        $page = new PageModel();
        $page->gestion($data,'taskerofarita-liste');
      //  $page->showme('user-liste',$data);
    }

    function ajout(){

        
      $data['page'] = self::$page;
      $data['table'] = self::$table;
      $data['view']['title'] = "Ajout d'une réalisation";
      $data['action'] = 'add';

      $session = \Config\Services::session();
      $p['userapp'] = $session->userapp;
      $p['useradmin'] = $session->useradmin;

    //  $data['r']['tftid'] = 0;
    //  $data['r']['tfdesc'] = "";
    //  $data['r']['tfduration'] = 0;

      $r = new class{};
      $r->tftid = 0;
      $r->tfdesc = '';
      $r->libelle = '';
      $r->tfduration = 0;
      $data['r'] = $r;

      $model = new TaskoModel();
      $data['ldtasko'] = $model->getTaskoDropDown($p);
 
      $page = new PageModel();
      $page->gestion($data,self::$page.'-edit');


  }
  function add(){
    $d = $this->addupd('a');
    $db = db_connect();
    $db->table('taskerofarita')->insert($d);
    $this->liste();
  }

  function edit($id){
    $session = \Config\Services::session();
    $data['page'] = self::$page;
    $data['view']['title'] = "Modification";
    $data['action'] = 'upd';

    $taskerofarita = new TaskerofaritaModel();
    $data['r'] =  $taskerofarita->get1taskerofarita($id);

    $d['userapp']=$session->userapp;
    $model = new TaskoModel();
    $data['ldtasko'] = $model->getTaskoDropDown($d);

    $page = new PageModel();
    $page->gestion($data,self::$page.'-edit');
}

public function upd() {
  $tfid = $this->request->getVar('tfid');
  $d = $this->addupd('u');
  $db = db_connect();
  $builder = $db->table(self::$table);
  $builder->where('tfid', $tfid);
  $builder->update($d);
  $this->liste();

}

  function addupd($t){
    $wudate = date('Y-m-d');
    $session = \Config\Services::session();
    $d['tftid'] = $this->request->getVar('tftid');
    $d['tfdesc'] = $this->request->getVar('tfdesc');
    $d['tfdate'] = $this->request->getVar('tfdate');
    $d['tfduration'] = $this->request->getVar('tfduration');
    $d['tfdatupd'] = $wudate;
    $d['tfusrupd'] = $session->userapp;
    if ($t=="a"){
        $d['tfdatcrt'] = $wudate;
        $d['tfusrcrt'] =  $session->userapp;
        }
    return $d;
    }

    function sup($id){

      $session = \Config\Services::session();
      $data['page'] = self::$page;
      $data['view']['title'] = "Suppression";
      $data['action'] = 'del';
  
      $taskerofarita = new TaskerofaritaModel();
      $data['r'] =  $taskerofarita->get1taskerofarita($id);
  
      $d['userapp']=$session->userapp;
      $model = new TaskoModel();
      $data['ldtasko'] = $model->getTaskoDropDown($d);

      $page = new PageModel();
      $page->gestion($data,self::$page.'-edit');
     
  }

/* ===== suppression reel ===== */
function del(){

  $id = $this->request->getVar('tfid');
  $db = db_connect();
  $builder = $db->table(self::$table);
  $builder->where('tfid', $id);
  $builder->delete();

  // mise à jour de la durée total réel de la tâche
  $id = $this->request->getVar('tftid');
  $model = new TaskoModel();
  $model->updDurationreal($id);
  $this->liste(); 

}

    public function crttab(){
      $db = db_connect();
      $strsql = "DROP TABLE `".$this->$dbtable."`;";
      echo '<br/>'.$strsql;
      $db = db_connect();
     $db->query($strsql);
$strsql = "CREATE TABLE `".$this->$dbtable."` (
      `tfid` INTEGER,
      `tftid` INTEGER,
      `tfuzanto` TEXT,
      `tfdate` TEXT,
      `tfdesc` TEXT,
      `tfduration` INTEGER,
      `tfdatcrt`	TEXT,
    `tfusrcrt`	TEXT,
    `tfdatupd`	TEXT,
    `tfusrupd`	TEXT,
       PRIMARY KEY(`tfid`));";
  echo '<br/>'.$strsql;

  $db->query($strsql);
  }

}