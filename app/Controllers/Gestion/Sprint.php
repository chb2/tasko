<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\PageModel;
use App\Models\ToolModel;
use App\Models\TaskoModel;
use App\Models\ParamModel;
use App\Models\SprintModel;

class Sprintr {
    public int    $id;
    public string $nom  = ''; 
    public string $desc = ''; 
    public string $goal = ''; 
    public string $datdeb = ''; 
    public string $datfin = ''; 
    public int    $nb_h_max = 0; 
  }

class Sprint extends BaseController {
    public static  $page = 'sprint';
    public static  $table = 'sprint';

    public function __construct()
    {
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
    }
 


    public function index()
	{
        $this->liste('param');
   
	}

    public function liste()
	{
        
        $data['view']['title'] = "Sprints";
        $data['table'] = self::$table;
        $data['page'] = self::$page;
        $session = \Config\Services::session();
        $p['userapp'] = $session->userapp;

        $sprint = new SprintModel();
        $data['t'] = $sprint->getSprintj($p);

        $t = $data['t'] ;


        $page = new PageModel();
        $page->gestion($data,'sprint-liste');

      //  $page->showme('user-liste',$data);
    }

    function ajout(){

        $session = \Config\Services::session();
        //  $data['dbtable'] = "tasko";
        $data['page'] = self::$page;
        $data['view']['title'] = "Ajout d'un sprint";
        $data['action'] = 'add';

        $r = new Sprintr();
        $data['r'] = $r;

        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }

    function add(){
        $d = $this->addupd('a');
       // print_r($d);
        $db = db_connect();
        $db->table('sprint')->insert($d);
        $this->liste();
	}

    function edit($id){
        $data['page'] = self::$page;
        $data['view']['title'] = "Modification d'un sprint";
        $data['action'] = 'upd';

        $sprint = new SprintModel();
        $data['r'] = $sprint->get1sprint($id);
    

        $page = new PageModel();
        $page->gestion($data,'sprint-edit');
   
    }

    public function upd() {
        $id = $this->request->getVar('id');
        echo $id;
        $d = $this->addupd('u');
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->update($d);
         $this->liste();
    }


    function addupd($t){
        $wudate = date('Y-m-d');
        $session = \Config\Services::session();

        $d['nom'] = $this->request->getVar('nom');
        $d['desc'] = $this->request->getVar('desc');
        $d['goal'] = $this->request->getVar('goal');
        $d['datdeb'] = $this->request->getVar('datdeb');
        $d['datfin'] = $this->request->getVar('datfin');
        $d['nb_h_max'] = $this->request->getVar('nb_h_max');
        $d['datupd'] = $wudate;
        $d['usrupd'] = $session->userapp;
        if ($t=="a"){
            $d['datcrt'] = $wudate;
            $d['usrcrt'] =  $session->userapp;
            }
        return $d;
        }

    function sup($id){
        $data['dbtable'] = "tasko";
        $data['page'] = self::$page;
       // $data['view']['title'] = "Modification d'une tache";
        $data['view']['title'] = "Suppression";
        $data['action'] = 'del';

        $projekto = new ProjektoModel();
        $data['r'] = $projekto->get1projekto($id);

        $param = new ParamModel();
        $data['categs'] = $param->getparams('categ');

        $tasko = new TaskoModel();
        $nb = $tasko->getNbTaskosProjekto($id);
        if ($nb > 0)    {
            $data['action'] = 'upd';
            $data['view']['title'] = "Modification";
            $data['erreurs'][] = "Suppression impossible : ce projet contient des taches (".$nb.")";
        }
        else
        {
            $data['action'] = 'del';
            $data['view']['title'] = "Suppression";
        }


        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }
    /* ===== suppression reel ===== */
    function del(){
        $id = $this->request->getVar('id');
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->delete();
        $this->liste();
    }

    public function vue($id)
    {
        $data['dbtable'] = "sprint";
        $data['view']['title'] = "Sprint";


        $sprint = new SprintModel();
        $p = $sprint->get1sprint($id);
        $data['p'] = $p;
 
        // état
        $tasko = new TaskoModel();

        // chercher les taches d'un sprint
        $t = $tasko->getTaskosSprint($id);
        $data['t'] = $t;



        $page = new PageModel();
        $page->gestion($data,'sprint-vue');
    }

    function calculordre(){

        //$session = \Config\Services::session();
        //$typevue = $session->typevue;
        $projekto = new ProjektoModel();
        $projekto->calculordre();
        $this->liste(); 
    
    }

    public function crttab()
	{
        $sprint = new SprintModel();
        $sprint->CreationTable();
	}

 

}
