<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\UzantoModel;
use App\Models\PageModel;
/*

A faire : mot de passe avec une force mini
Demande de changement de mot de passe
Ajouter champs
- date de validation de l'email (pour envoi de mail de contrôle une fois par an)
- Clé de réactivation
- Date limite de clé
Demande de mot de passe : Génération d'une clé et mise à jour avec date de la clé --> envoi du lien
Clic sur le lien : vérificaiton de la clé et de la date limite 

faire une routine de contrôle de création login avec quelques rêgles
- mini 3 caractères
- AZ az 09 et quelques caractères _- pas d'accent

*/

class Uzanto extends BaseController {
    public static  $page = 'uzanto';
    public static  $table = 'uzanto';

    public function __construct()
    {
        // debut code à ajouter dans construc pour langue
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        $lang= $session->lang ;
        $language = \Config\Services::language();
        $language->setLocale($lang); 
        // fin code à ajouter dans construc pour langue


        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
    }
  

    public function index()
	{
        $this->liste();
	}

    public function langen()
    {
             $session = \Config\Services::session();
             $session->lang = 'en';
             $this->index();
    }
           public function langfr()
    {
             $session = \Config\Services::session();
             $session->lang = 'fr';
             $this->index();
    }

    public function test()
	{
        $data['view']['title'] = "Utilisateurs";
        $data['typeapp'] = "pordego";
        $data['dbtable'] = "uzanto";
        $strsql = "select * from uzanto";

        $db = db_connect();
        $query = $db->query($strsql);
        $data['t'] = $query->getResult();
        $page = new Page();
        $page->admin('uzanto-liste',$data);
	}
    public function liste()
	{
        $session = \Config\Services::session();
        $lang= $session->lang ;
       // $lang='en';
        $language = \Config\Services::language();
        $language->setLocale($lang); 


        $data['view']['title'] = "Utilisateurs";
        $data['table'] = self::$table;
        $data['page'] = self::$page;
        // a remplacer par une méthode getuzantoj('id')
        $strsql = "select * from uzanto order by id";
        $db = db_connect();
        $query = $db->query($strsql);
       $data['t'] = $query->getResult();

       $uzanto = new UzantoModel();
       $data['uzantoj'] = $uzanto->getuzantoj();

        $page = new PageModel();
        $page->gestion($data,'uzanto-liste');
      //  $page->showme('user-liste',$data);
    }
    
    public function ajout()
    {
          $data['view']['title'] = "Utilisateurs";
          $data['action'] = 'add';
          $data['page'] = self::$page;

          // initialisaiton des champs du formulaire
          $r = new class{};
          $r->code = '';
          $r->libelle = '';
          $r->email = '';
          $r->pasvor = '';
          $r->grpaut = '';
          $r->remarque = '';
          $r->datvalemail = '0001-01-01';
          $data['r'] = $r;


          $page = new PageModel();
          $page->gestion($data,self::$page.'-edit');
    }


    function add(){
        $d = $this->addupd('a');
        $db = db_connect();
        $db->table(self::$table)->insert($d);
        $this->liste();
    }

      function edit($id){
        $data['page'] = self::$page;
        $data['view']['title'] = "Modification utilisateur";
        $data['action'] = 'upd';

        $uzanto = new UzantoModel();
        $data['r'] = $uzanto->get1uzanto($id);


        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }
    public function upd() {
        $id = $this->request->getVar('id');
        //echo $id;
        $d = $this->addupd('u');
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->update($d);
        $this->liste();
    }
       
    function addupd($t){
 
        //$session = \Config\Services::session();
        $d['code'] = $this->request->getVar('code');
        $d['libelle'] = $this->request->getVar('libelle');
        $d['email'] = $this->request->getVar('email');

        if ($this->request->getVar('pasvor') > " "){
            $d['pasvor'] = md5($this->request->getVar('pasvor'));
        }
        $d['grpaut'] = $this->request->getVar('grpaut');
        $d['remarque'] = $this->request->getVar('remarque');
        $d['usrmod'] = 'usrupd';
        $d['datmod'] = date('Y-m-d H:m');
        if ($t=="a"){
              $d['usrcrt'] = 'user';
            }
        
        // changement email 
        if ($this->request->getVar('email') <> $this->request->getVar('emailold') and $this->request->getVar('emailold') > ''){
            $d['datvalemail'] = '0001-01-01';
            $d['slosilo'] = md5(date('Y-m-d H:m:s'));
            $uzanto = new UzantoModel();
            $p['id'] = $this->request->getVar('id');
            $p['emailold'] = $this->request->getVar('emailold');
            $p['email'] = $this->request->getVar('email');
            $p['slosilo'] = $d['slosilo'];
            $d['remarque'] = 'Changement email : ancien ('.$this->request->getVar('emailold').')'.$this->request->getVar('remarque');
            $uzanto->chgemail($p);
        }
        return $d;
        }


    public function crttab()
	{
        #Création de la table. A l'avenir il faudra faire une création des table avec un autre controleur 

        #$this->liste();
        $uzanto = new UzantoModel();
        $uzanto->CreationTable();
	}
    public function updtab1()
	{
        #Création de la table. A l'avenir il faudra faire une création des table avec un autre controleur 

        #$this->liste();
        $uzanto = new UzantoModel();
        $uzanto->addremarquefield();
	}

}