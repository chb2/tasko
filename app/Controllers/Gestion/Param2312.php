<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\ParamModel;
use App\Models\PageModel;


class Param extends BaseController {
    public static  $page = 'param';
    public static  $table = 'param';

    public function __construct()
    {
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
    }
  

    public function index()
	{
        $this->liste('param');
    
	}
    public function liste($paty)
	{
        $data['view']['title'] = "Paramètres ".$paty;
        $data['table'] = self::$table;
        $data['page'] = self::$page;
        $data['paty'] = $paty;

        $param = new ParamModel();
        $data['t'] = $param->getParams($paty);

        $page = new PageModel();
        $page->gestion($data,'param-liste');
    }

    public function ajout($paty)
    {
        $data['view']['title'] = "Paramètres ".$paty;
          $data['action'] = 'add';
          $data['page'] = self::$page;
   
      //  echo $paty;
          // initialisaiton des champs du formulaire
          $r = new class{};
          $r->paty = $paty;
          $r->paco = '';
          $r->libelle = '';
          $r->zona = '';
         $data['r'] = $r;

          $page = new PageModel();
          $page->gestion($data,self::$page.'-edit');
    }
    function add(){
        $paty = $this->request->getVar('paty');
        //echo 'add'.$paty;
        $d = $this->addupd('a');
        $db = db_connect();
        $db->table(self::$table)->insert($d);
        $this->liste($paty);
    }


    function edit($id){
        $data['page'] = self::$page;
        $data['view']['title'] = "Modification";
        $data['action'] = 'upd';

        $param = new ParamModel();
        $data['r'] = $param->get1param($id);


        $page = new PageModel();
        $page->gestion($data,self::$page.'-edit');
    }

    public function upd() {
        $id = $this->request->getVar('id');
      //  echo $id;
        $d = $this->addupd('u');
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->update($d);
        $paty = $d['paty'];
        $this->liste($paty);
    }

    function addupd($t){
 
        $session = \Config\Services::session();
        $d['paty'] = $this->request->getVar('paty');
        $d['paco'] = $this->request->getVar('paco');
        $d['libelle'] = $this->request->getVar('libelle');
        $d['zona'] = $this->request->getVar('zona');
 
 
        $d['usrmod'] = $session->userapp;
        $d['datmod'] = date('Y-m-d H:m');
        if ($t=="a"){
              $d['usrcrt'] = $session->userapp;
            }
        return $d;
        }

/* ===== suppression etape 1 ===== */
function sup($id){

    $data['page'] = self::$page;
    $data['view']['title'] = "Modification";
    $data['action'] = 'del';

    $param = new ParamModel();
    $data['r'] = $param->get1param($id);


    $page = new PageModel();
    $page->gestion($data,self::$page.'-edit');


}

/* ===== suppression reel ===== */
 function del(){

    $id = $this->request->getVar('id');
    $paty = $this->request->getVar('paty');
    $db = db_connect();
    $builder = $db->table(self::$table);
    $builder->where('id', $id);
    $builder->delete();
    $this->liste($paty);

}
public function crttab()
{
        $param = new ParamModel();
        $param->CreationTable();
}

}