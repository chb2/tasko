<?php namespace App\Controllers\Gestion;
use \App\Controllers\BaseController;
use App\Models\UtilecoModel;
use App\Models\PageModel;
use App\Models\InstallModel;
/*



*/

class Utileco extends BaseController {
    public static  $page = 'utileco';
  //  public static  $table = 'uzanto';

    public function __construct()
    {
       $session = \Config\Services::session();
        $userapp = $session->userapp;
       if($userapp == false || $userapp  = NULL || empty($userapp ))
        {

            $data['msgerr'] = "";
            $page = new PageModel();
            $page->gestion($data,'login');
            exit;
        }
    }

    public function index()
	{
        $data['view']['title'] = "Outils";
        $data['page'] = self::$page;
        $page = new PageModel();
        $page->gestion($data,'utilecov');
	}

    public function installation_preparation()
	{
       $data['view']['title'] = "préparation installation";
       $data['msgerr'] = "Attention cette action va réinitialiser l'application, vous allez perdre toutes vos données. Avez vous fait une sauvegarde ? ";
       $data['action'] = "installation_valide";
       $data['txtbouton'] = "Valider";
       $data['classalert'] = "alert-warning";
       $page = new PageModel(); 
       $page->gestion($data,'utileco-action');
	}


    public function installation_valide()
	{
        $data['view']['title'] = "Préaration installation OK";
        $data['msgerr'] = "La préparation de l'installation a été faite avec succes";
        $data['txtbouton'] = "Retour";
        $data['classbouton'] = "btn-block btn-success";
        $data['classalert'] = "alert-primary";
        $data['action'] = "";

        $install  = new InstallModel(); 
        $install->install_preparation();
        
        $page = new PageModel(); 
        $page->gestion($data,'utileco-action');
	}
 
    
 
 

}
