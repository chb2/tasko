<?php 

namespace App\Controllers;
use \App\Controllers\BaseController;
use App\Models\InstallModel;
use App\Models\ParamModel;
use App\Models\UzantoModel;
use App\Models\PageModel;
use App\Models\LogModel;


class Install extends BaseController
{


    public function __construct()
    {
        $install = new InstallModel();
        $ok = $install->installchk();
        if ($ok == 1)
        {
            $this->partie1controle();
            exit;
        }
    }
  
    public function index()
	{
         $this->partie1controle();   
	}

public function partie1controle()
	{
        
        $install = new InstallModel();
        $ok = $install->installchk();
        if ($ok == 1)
        {

            $data['view']['title'] = "Installation";
            $data['page'] = "install";
            $data['action'] = "installok";
            $data['msg'] = "L'installation est bonne";
             $page = new PageModel();
             $page->page($data);
        }
        else
        {
            $this->partie2saisieinfo();  

        }

        // contrôles 
        // si installation a déjà eu lieu ou pas
    }

    public function partie2saisieinfo()
	{
        $data['view']['title'] = "Installation";
        $data['page'] = "install";
        $data['action'] = "install-crtadmin-edit";
        $data['msg'] = "Création d'un utilisateur administrateur";
         $page = new PageModel();
         $page->page($data);
    }

    public function partie3installation()
	{
        
        $log = new LogModel();
        $log->CreationTable();

        $param = new ParamModel();
        $param->CreationTable();


        $uzanto = new UzantoModel();
        $uzanto->CreationTable();

        // la création uzanto devra se faire dans le modèle
        $u = $this->request->getVar('u');
        $d['code'] = $this->request->getVar('u');
        $d['pasvor'] = md5($this->request->getVar('p'));
        $d['usrcrt'] = $this->request->getVar('u');
        $d['usrmod'] = $this->request->getVar('u');
        $d['grpaut'] = 'A';
        $db = db_connect();
        $db->table('uzanto')->insert($d);


         $data['view']['title'] = "Installation";
         $data['page'] = "install";
         $data['action'] = "installok";
         $data['msg'] = "L'installation a bien été faite";
         $data['msg'] .= "<br/>Utilisation.".$u." créé en tant qu'admin";
          $page = new PageModel();
          $page->page($data);
    }
    
    
}
